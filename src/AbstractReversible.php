<?php

declare(strict_types=1);

namespace CoStack\Reversible;

/**
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
abstract class AbstractReversible implements Reversible
{
    /**
     * @param mixed $value
     * @return mixed
     */
    public function execute($value)
    {
        return $this->getExecutionClosure()($value);
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    public function reverse($value)
    {
        return $this->getReversionClosure()($value);
    }
}
