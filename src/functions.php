<?php

declare(strict_types=1);

namespace CoStack\Reversible;

use CoStack\Reversible\Exception\InvalidArgumentTypeException;
use CoStack\Reversible\Exception\JsonDecodeException;
use CoStack\Reversible\Exception\JsonEncodeException;

use function gettype;
use function is_array;
use function is_object;
use function is_scalar;
use function json_decode;
use function json_encode;
use function json_last_error;
use function json_last_error_msg;

use const JSON_ERROR_NONE;

/**
 * Required before PHP 7.3 to throw an Exception if decoding fails.
 * Also, this version defaults to associative=true.
 *
 * @param string $json The json string being decoded.
 * @param bool $associative When TRUE, returned objects will be converted into associative arrays.
 * @param int $depth User specified recursion depth.
 * @param int $flags Bitmask of JSON decode options
 * @return array|bool|int|float|string|object
 * @throws JsonDecodeException
 * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
 */
function json_decode_assoc(string $json, bool $associative = true, int $depth = 512, int $flags = 0)
{
    /** @var array|bool|int|float|string|object $data */
    $data = json_decode($json, $associative, $depth, $flags);
    $jsonError = json_last_error();
    if (JSON_ERROR_NONE !== $jsonError) {
        throw new JsonDecodeException($jsonError, json_last_error_msg(), $json);
    }

    return $data;
}

/**
 * Required before PHP 7.3 to throw an Exception if encoding fails.
 *
 * @param array|object|scalar $json
 * @param int $depth
 * @param int $flags
 * @return string
 * @throws InvalidArgumentTypeException
 * @throws JsonEncodeException
 * @psalm-suppress DocblockTypeContradiction
 */
function json_encode_strict($json, int $depth = 512, int $flags = 0): string
{
    if (!is_scalar($json) && !is_array($json) && !is_object($json)) {
        throw new InvalidArgumentTypeException('json', 'array|object|scalar', gettype($json));
    }
    $data = json_encode($json, $flags, $depth);
    $jsonError = json_last_error();
    if (JSON_ERROR_NONE !== $jsonError) {
        throw new JsonEncodeException($jsonError, json_last_error_msg(), $json);
    }

    return $data;
}
