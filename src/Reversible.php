<?php

declare(strict_types=1);

namespace CoStack\Reversible;

use Closure;

interface Reversible
{
    /**
     * @param mixed $value
     * @return mixed
     */
    public function execute($value);

    public function getExecutionClosure(): Closure;

    /**
     * @param mixed $value
     * @return mixed
     */
    public function reverse($value);

    public function getReversionClosure(): Closure;
}
