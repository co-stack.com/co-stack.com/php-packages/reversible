<?php

declare(strict_types=1);

namespace CoStack\Reversible\Operation\Compression;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Exception\RangeException;

use function gzdeflate;
use function gzinflate;

/**
 * @method string execute($value)
 * @method string reverse($value)
 */
class GzipCompression extends AbstractReversible
{
    /** @var int */
    private $level;

    /**
     * @param int $level The level of compression. Can be given as 0 for no compression up to 9 for maximum compression.
     *  If not given, the default compression level will be the default compression level of the zlib library.
     * @throws RangeException
     */
    public function __construct(int $level = -1)
    {
        if ($level < -1 || $level > 9) {
            throw new RangeException($level, 1, 9);
        }
        $this->level = $level;
    }

    public function getExecutionClosure(): Closure
    {
        $level = $this->level;
        return static function (string $string) use ($level): string {
            return gzdeflate($string, $level);
        };
    }

    public function getReversionClosure(): Closure
    {
        return static function (string $string): string {
            return gzinflate($string);
        };
    }
}
