<?php

declare(strict_types=1);

namespace CoStack\Reversible\Operation\Transform;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Exception\ArrayIsNotSequentialException;
use CoStack\Reversible\Exception\EmptyStringException;
use CoStack\Reversible\TypeLossy;

use function array_filter;
use function array_keys;
use function explode;
use function implode;

/**
 * @method string execute($value)
 * @method array reverse($value)
 */
class ImplodeTransform extends AbstractReversible implements TypeLossy
{
    /** @var non-empty-string */
    private $delimiter;

    /**
     * @param non-empty-string $delimiter
     * @throws EmptyStringException
     */
    public function __construct(string $delimiter = '|')
    {
        if (empty($delimiter)) {
            throw new EmptyStringException(0, __METHOD__);
        }
        $this->delimiter = $delimiter;
    }

    public function getExecutionClosure(): Closure
    {
        return
            /**
             * @param array<string> $value
             * @throws ArrayIsNotSequentialException
             */
            function (array $value): string {
                if (!empty(array_filter(array_keys($value), 'is_string'))) {
                    throw new ArrayIsNotSequentialException(__METHOD__);
                }
                return implode($this->delimiter, $value);
            };
    }

    public function getReversionClosure(): Closure
    {
        return function (string $value): array {
            return explode($this->delimiter, $value);
        };
    }
}
