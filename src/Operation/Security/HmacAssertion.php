<?php

declare(strict_types=1);

namespace CoStack\Reversible\Operation\Security;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Exception\HmacAssertionFailedException;

use function hash_equals;
use function hash_hmac;
use function strpos;
use function substr;

/**
 * @method string execute($value)
 * @method string reverse($value)
 */
class HmacAssertion extends AbstractReversible
{
    /**
     * @var string
     */
    private $key;
    /**
     * @var string
     */
    private $algo;
    /**
     * @var string
     */
    private $splitCharacter;

    public function __construct(string $key, string $algo = 'sha1', string $splitCharacter = ':')
    {
        $this->key = $key;
        $this->algo = $algo;
        $this->splitCharacter = $splitCharacter;
    }

    public function getExecutionClosure(): Closure
    {
        return function (string $value): string {
            return hash_hmac($this->algo, $value, $this->key) . $this->splitCharacter . $value;
        };
    }

    public function getReversionClosure(): Closure
    {
        return function (string $value): string {
            $splitPosition = strpos($value, $this->splitCharacter);
            if (false === $splitPosition) {
                throw new HmacAssertionFailedException();
            }
            $userHmac = substr($value, 0, $splitPosition);
            $value = substr($value, $splitPosition + 1);
            $knownHmac = hash_hmac($this->algo, $value, $this->key);
            if (!hash_equals($knownHmac, $userHmac)) {
                throw new HmacAssertionFailedException();
            }
            return $value;
        };
    }
}
