<?php

/**
 * @noinspection PhpElementIsNotAvailableInCurrentPhpVersionInspection
 */

declare(strict_types=1);

namespace CoStack\Reversible\Operation\Security;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Exception\InvalidEncryptionKeyException;
use CoStack\Reversible\Exception\InvalidPrivateKeyException;
use CoStack\Reversible\Exception\InvalidPublicKeyException;
use CoStack\Reversible\Exception\JsonDecodeException;
use CoStack\Reversible\Exception\SignatureAssertionErrorException;
use CoStack\Reversible\Exception\SignatureAssertionFailedException;
use CoStack\Reversible\Exception\SigningFailedException;
use CoStack\Reversible\Operation\AsymmetricKeys;
use CoStack\Reversible\Operation\OpenSSL;
use OpenSSLAsymmetricKey;
use OpenSSLCertificate;

use function base64_decode;
use function base64_encode;
use function CoStack\Reversible\json_decode_assoc;
use function CoStack\Reversible\json_encode_strict;
use function count;
use function error_reporting;
use function is_array;
use function is_string;
use function openssl_sign;
use function openssl_verify;
use function restore_error_handler;
use function set_error_handler;

use const OPENSSL_ALGO_SHA512;

/**
 * @method string execute($value)
 * @method string reverse($value)
 */
class SignatureAssertion extends AbstractReversible
{
    use AsymmetricKeys;
    use OpenSSL;

    private const OPENSSL_INVALID_PRIVATE_KEY_ERROR = 'openssl_sign(): supplied key param cannot be coerced into a private key';
    private const OPENSSL_INVALID_PUBLIC_KEY_ERROR = 'openssl_verify(): supplied key param cannot be coerced into a public key';

    /** @var int */
    private $algorithm;

    /**
     * @param OpenSSLAsymmetricKey|OpenSSLCertificate|array|string|null $privateKey Only required if you want to reverse
     * @param OpenSSLAsymmetricKey|OpenSSLCertificate|array|string|null $publicKey Only required if you want to execute
     * @param int $algorithm The algorithm to use as represented by one of the OPENSSL_ALGO_* constants.
     * @throws InvalidEncryptionKeyException
     */
    public function __construct($privateKey = null, $publicKey = null, int $algorithm = OPENSSL_ALGO_SHA512)
    {
        $this->assertValidEncryptionKey('private', $privateKey);
        $this->assertValidEncryptionKey('public', $publicKey);
        $this->privateKey = $privateKey;
        $this->publicKey = $publicKey;
        $this->algorithm = $algorithm;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter) Error handler signature has to have $code
     */
    public function getExecutionClosure(): Closure
    {
        return function (string $string): string {
            set_error_handler(
            /** @psalm-suppress UnusedClosureParam */
                function (int $code, string $message): bool {
                    if (strtolower($message) === self::OPENSSL_INVALID_PRIVATE_KEY_ERROR) {
                        $errors = $this->collectOpenSslErrors();
                        restore_error_handler();
                        throw new InvalidPrivateKeyException($errors, $this->privateKey);
                    }
                    // @codeCoverageIgnoreStart
                    return 0 === error_reporting();
                    // @codeCoverageIgnoreEnd
                }
            );
            /** @psalm-suppress PossiblyInvalidArgument */
            $success = openssl_sign($string, $signature, $this->privateKey, $this->algorithm);
            restore_error_handler();
            $errors = $this->collectOpenSslErrors();

            if (!$success) {
                // @codeCoverageIgnoreStart
                throw new SigningFailedException($errors);
                // @codeCoverageIgnoreEnd
            }
            return base64_encode(json_encode_strict([$string, base64_encode($signature)]));
        };
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.UnusedFormalParameter) Error handler signature has to have $code
     */
    public function getReversionClosure(): Closure
    {
        return function (string $string): string {
            $string = base64_decode($string, true);
            if (false === $string) {
                throw new SignatureAssertionFailedException();
            }
            try {
                $stringParts = json_decode_assoc($string);
            } catch (JsonDecodeException $exception) {
                throw new SignatureAssertionFailedException($exception);
            }
            if (!is_array($stringParts) || 2 !== count($stringParts)) {
                throw new SignatureAssertionFailedException();
            }
            [$data, $signature] = $stringParts;
            if (!is_string($data) || !is_string($signature)) {
                throw new SignatureAssertionFailedException();
            }
            $signature = base64_decode($signature, true);
            if (false === $signature) {
                throw new SignatureAssertionFailedException();
            }

            set_error_handler(
            /** @psalm-suppress UnusedClosureParam */
                function (int $code, string $message): bool {
                    if (strtolower($message) === self::OPENSSL_INVALID_PUBLIC_KEY_ERROR) {
                        $errors = $this->collectOpenSslErrors();
                        restore_error_handler();
                        throw new InvalidPublicKeyException($errors, $this->privateKey);
                    }
                    // @codeCoverageIgnoreStart
                    return 0 === error_reporting();
                    // @codeCoverageIgnoreEnd
                }
            );
            /** @psalm-suppress PossiblyInvalidArgument */
            $validity = openssl_verify($data, $signature, $this->publicKey, $this->algorithm);
            restore_error_handler();
            $errors = $this->collectOpenSslErrors();

            if (0 === $validity) {
                throw new SignatureAssertionFailedException();
            }
            if (-1 === $validity) {
                // @codeCoverageIgnoreStart
                throw new SignatureAssertionErrorException($errors);
                // @codeCoverageIgnoreEnd
            }
            return $data;
        };
    }
}
