<?php

declare(strict_types=1);

namespace CoStack\Reversible\Operation;

use function openssl_error_string;

trait OpenSSL
{
    /**
     * Collects all errors from openssl.
     * Some openssl functions produce errors, even when the function call was successful.
     * @see https://www.php.net/manual/en/function.openssl-verify.php#122307
     *
     * Calling this method without using the return value will simply flush the current queue
     *
     * @return array<string>
     */
    private function collectOpenSslErrors(): array
    {
        $errors = [];
        while ($error = openssl_error_string()) {
            $errors[] = $error;
        }
        return $errors;
    }
}
