<?php

declare(strict_types=1);

namespace CoStack\Reversible\Operation\Encryption;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Exception\DecryptionFailedException;

use function base64_decode;
use function base64_encode;
use function CoStack\Reversible\json_decode_assoc;
use function CoStack\Reversible\json_encode_strict;
use function is_array;
use function is_string;
use function random_bytes;

/**
 * @method string execute($value)
 * @method string reverse($value)
 */
class HybridEncryption extends AbstractReversible
{
    private const ERROR_INVALID_INPUT = 'The input value is invalid';

    /** @var AsymmetricEncryption */
    private $asymmetricEncryption;

    /** @var positive-int */
    private $bytes;

    /**
     * @param AsymmetricEncryption $asymmetricEncryption Always required
     * @param positive-int $bytes
     */
    public function __construct(AsymmetricEncryption $asymmetricEncryption, int $bytes = 128)
    {
        $this->asymmetricEncryption = $asymmetricEncryption;
        $this->bytes = $bytes;
    }

    public function getExecutionClosure(): Closure
    {
        return function (string $string): string {
            $secret = random_bytes($this->bytes);
            // Encrypt Data symmetric
            $encrypted = (new SymmetricEncryption($secret))->execute($string);
            // Encrypt symmetric key asymmetrically
            $encryptedSecret = base64_encode($this->asymmetricEncryption->execute($secret));
            // Return both encrypted data and key
            return json_encode_strict([$encrypted, $encryptedSecret]);
        };
    }

    public function getReversionClosure(): Closure
    {
        return function (string $string): string {
            // Split encrypted data and symmetric key
            $decoded = json_decode_assoc($string);
            if (!is_array($decoded)) {
                throw new DecryptionFailedException([self::ERROR_INVALID_INPUT]);
            }
            [$encrypted, $encryptedSecret] = $decoded;
            if (!is_string($encrypted) || !is_string($encryptedSecret)) {
                throw new DecryptionFailedException([self::ERROR_INVALID_INPUT]);
            }
            // Decrypt symmetric key asymmetrically
            $secret = $this->asymmetricEncryption->reverse(base64_decode($encryptedSecret));
            // Decrypt data with decrypted symmetric key
            return (new SymmetricEncryption($secret))->reverse($encrypted);
        };
    }
}
