<?php

/**
 * @noinspection PhpElementIsNotAvailableInCurrentPhpVersionInspection
 */

declare(strict_types=1);

namespace CoStack\Reversible\Operation\Encryption;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Exception\DecryptionFailedException;
use CoStack\Reversible\Exception\InvalidEncryptionKeyException;
use CoStack\Reversible\Exception\InvalidPrivateKeyException;
use CoStack\Reversible\Exception\InvalidPublicKeyException;
use CoStack\Reversible\Exception\MissingPrivateKeyException;
use CoStack\Reversible\Exception\MissingPublicKeyException;
use CoStack\Reversible\Operation\AsymmetricKeys;
use CoStack\Reversible\Operation\OpenSSL;
use OpenSSLAsymmetricKey;
use OpenSSLCertificate;

use function error_reporting;
use function openssl_private_decrypt;
use function openssl_public_encrypt;
use function restore_error_handler;
use function set_error_handler;

use const OPENSSL_PKCS1_OAEP_PADDING;

/**
 * This operation execution returns binary code. You should probably wrap it with a Base64Encoding if you want to
 * transfer the results over a sensitive medium.
 *
 * @method string execute($value)
 * @method string reverse($value)
 */
class AsymmetricEncryption extends AbstractReversible
{
    use AsymmetricKeys;
    use OpenSSL;

    private const OPENSSL_INVALID_PUBLIC_KEY_ERROR = 'openssl_public_encrypt(): key parameter is not a valid public key';
    private const OPENSSL_INVALID_PRIVATE_KEY_ERROR = 'openssl_private_decrypt(): key parameter is not a valid private key';

    /**
     * @param OpenSSLAsymmetricKey|OpenSSLCertificate|array|string|null $privateKey Only required if you want to reverse
     * @param OpenSSLAsymmetricKey|OpenSSLCertificate|array|string|null $publicKey Only required if you want to execute
     * @throws InvalidEncryptionKeyException
     */
    public function __construct($privateKey = null, $publicKey = null)
    {
        $this->assertValidEncryptionKey('private', $privateKey);
        $this->assertValidEncryptionKey('public', $publicKey);
        $this->privateKey = $privateKey;
        $this->publicKey = $publicKey;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter) Error handler signature has to have $code
     */
    public function getExecutionClosure(): Closure
    {
        return function (string $string): string {
            if (null === $this->publicKey) {
                throw new MissingPublicKeyException();
            }

            set_error_handler(
            /** @psalm-suppress UnusedClosureParam */
                function (int $code, string $message) {
                    if ($message === self::OPENSSL_INVALID_PUBLIC_KEY_ERROR) {
                        restore_error_handler();
                        $errors = $this->collectOpenSslErrors();
                        throw new InvalidPublicKeyException($errors, $this->publicKey);
                    }
                    // @codeCoverageIgnoreStart
                    return 0 === error_reporting();
                    // @codeCoverageIgnoreEnd
                }
            );

            /** @psalm-suppress PossiblyInvalidArgument */
            openssl_public_encrypt($string, $encrypted, $this->publicKey, OPENSSL_PKCS1_OAEP_PADDING);
            restore_error_handler();
            // Flush any error
            $this->collectOpenSslErrors();
            return $encrypted;
        };
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter) Error handler signature has to have $code
     */
    public function getReversionClosure(): Closure
    {
        return function (string $string): string {
            if (null === $this->privateKey) {
                throw new MissingPrivateKeyException();
            }

            set_error_handler(
                /** @psalm-suppress UnusedClosureParam */
                function (int $code, string $message) {
                    if (strtolower($message) === self::OPENSSL_INVALID_PRIVATE_KEY_ERROR) {
                        restore_error_handler();
                        $errors = $this->collectOpenSslErrors();
                        throw new InvalidPrivateKeyException($errors, $this->privateKey);
                    }
                    // @codeCoverageIgnoreStart
                    return 0 === error_reporting();
                    // @codeCoverageIgnoreEnd
                }
            );
            /** @psalm-suppress PossiblyInvalidArgument */
            openssl_private_decrypt($string, $decrypted, $this->privateKey, OPENSSL_PKCS1_OAEP_PADDING);
            restore_error_handler();
            $errors = $this->collectOpenSslErrors();

            /** @psalm-suppress TypeDoesNotContainNull Type can contain null if the private key is a valid key but can not decrypt the value */
            if (null === $decrypted) {
                throw new DecryptionFailedException($errors);
            }
            return $decrypted;
        };
    }
}
