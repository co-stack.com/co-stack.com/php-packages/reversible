<?php

declare(strict_types=1);

namespace CoStack\Reversible\Operation\Encryption;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Exception\DecryptionFailedException;
use CoStack\Reversible\Exception\EmptyStringException;
use CoStack\Reversible\Exception\UnsupportedCipherMethodException;
use CoStack\Reversible\Operation\OpenSSL;

use function array_map;
use function explode;
use function implode;
use function in_array;
use function openssl_cipher_iv_length;
use function openssl_decrypt;
use function openssl_encrypt;
use function openssl_get_cipher_methods;
use function openssl_random_pseudo_bytes;

/**
 * @method string execute($value)
 * @method string reverse($value)
 */
class SymmetricEncryption extends AbstractReversible
{
    use OpenSSL;

    /** @var string */
    private $secret;

    /** @var string */
    private $algo;

    /** @var non-empty-string */
    private $glue;

    /**
     * @param string $secret The secret string which is used to encrypt and decrypt the message
     * @param string $algo The algorithm for en/decryption. Run `openssl_get_cipher_methods()` to see your available ones
     * @param non-empty-string $glue The glue used to separate the encrypted string and iv in the result
     * @throws UnsupportedCipherMethodException
     * @throws EmptyStringException
     */
    public function __construct(string $secret, string $algo = 'aes-256-gcm', string $glue = '|')
    {
        $methods = openssl_get_cipher_methods();
        if (!in_array($algo, $methods, true)) {
            throw new UnsupportedCipherMethodException($algo, $methods);
        }
        $this->secret = $secret;
        $this->algo = $algo;
        if (empty($glue)) {
            throw new EmptyStringException(0, __METHOD__);
        }
        $this->glue = $glue;
    }

    public function getExecutionClosure(): Closure
    {
        return function (string $string): string {
            $vector = openssl_random_pseudo_bytes(openssl_cipher_iv_length($this->algo));
            $tag = '';
            $encrypted = openssl_encrypt($string, $this->algo, $this->secret, 0, $vector, $tag);
            return implode($this->glue, array_map('base64_encode', [$vector, $tag, $encrypted]));
        };
    }

    public function getReversionClosure(): Closure
    {
        return function (string $string): string {
            [$vector, $tag, $encrypted] = array_map('base64_decode', explode($this->glue, $string));
            $decrypted = openssl_decrypt($encrypted, $this->algo, $this->secret, 0, $vector, $tag);
            if (false === $decrypted) {
                $errors = $this->collectOpenSslErrors();
                throw new DecryptionFailedException($errors);
            }
            return $decrypted;
        };
    }
}
