<?php

/**
 * @noinspection PhpElementIsNotAvailableInCurrentPhpVersionInspection
 */

declare(strict_types=1);

namespace CoStack\Reversible\Operation;

use CoStack\Reversible\Exception\InvalidEncryptionKeyException;
use OpenSSLAsymmetricKey;
use OpenSSLCertificate;

use function class_exists;
use function is_array;
use function is_object;
use function is_string;

trait AsymmetricKeys
{
    /** @var OpenSSLAsymmetricKey|OpenSSLCertificate|array|string|null */
    private $privateKey;

    /** @var OpenSSLAsymmetricKey|OpenSSLCertificate|array|string|null */
    private $publicKey;

    /**
     * @param string $usage
     * @param OpenSSLAsymmetricKey|OpenSSLCertificate|array|string|null $key
     * @throws InvalidEncryptionKeyException
     * @psalm-suppress RedundantConditionGivenDocblockType
     * @psalm-suppress UnusedParam
     * @psalm-suppress UndefinedClass
     * @noinspection NotOptimalIfConditionsInspection
     */
    private function assertValidEncryptionKey(string $usage, $key): void
    {
        if (null === $key || is_string($key) || is_array($key)) {
            return;
        }
        if (is_object($key)) {
            // @codeCoverageIgnoreStart
            if (class_exists(OpenSSLCertificate::class) && $key instanceof OpenSSLCertificate) {
                return;
            }
            if (class_exists(OpenSSLAsymmetricKey::class) && $key instanceof OpenSSLAsymmetricKey) {
                return;
            }
            // @codeCoverageIgnoreEnd
        }
        throw new InvalidEncryptionKeyException(
            $usage,
            'OpenSSLAsymmetricKey|OpenSSLCertificate|array|string|null',
            $key
        );
    }
}
