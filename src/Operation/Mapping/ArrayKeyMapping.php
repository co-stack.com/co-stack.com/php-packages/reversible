<?php

declare(strict_types=1);

namespace CoStack\Reversible\Operation\Mapping;

use Closure;
use CoStack\Reversible\AbstractReversible;

use function array_flip;
use function array_key_exists;
use function ksort;

/**
 * @method array execute($value)
 * @method array reverse($value)
 */
class ArrayKeyMapping extends AbstractReversible
{
    private $mapping;

    /**
     * @param array $mapping Key = Numeric, Value = assoc index
     */
    public function __construct(array $mapping)
    {
        $this->mapping = $mapping;
    }

    public function getExecutionClosure(): Closure
    {
        return function (array $value): array {
            $mapping = array_flip($this->mapping);
            $newValue = [];
            /**
             * @var int|string $key
             * @var mixed $val
             */
            foreach ($value as $key => $val) {
                /**
                 * @psalm-suppress MixedArrayOffset Array keys are always int|string
                 * @psalm-suppress MixedAssignment The mixed assignment is desired
                 */
                $newValue[$mapping[$key] ?? $key] = $val;
            }
            ksort($newValue);
            return $newValue;
        };
    }

    public function getReversionClosure(): Closure
    {
        return function (array $value): array {
            $newValue = [];
            /**
             * @var int|string $key
             * @var mixed $val
             */
            foreach ($value as $key => $val) {
                if (array_key_exists($key, $this->mapping)) {
                    /** @var mixed $key */
                    $key = $this->mapping[$key];
                }
                /**
                 * @psalm-suppress MixedArrayOffset Array keys are always int|string
                 * @psalm-suppress MixedAssignment The mixed assignment is desired
                 */
                $newValue[$key] = $val;
            }
            return $newValue;
        };
    }
}
