<?php

declare(strict_types=1);

namespace CoStack\Reversible\Operation\Fixed;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Exception\UnexpectedPrefixException;

use function strlen;
use function substr;

/**
 * @method string execute($value)
 * @method string reverse($value)
 */
class FixedPrefix extends AbstractReversible
{
    /**
     * @var string
     */
    private $prefix;

    public function __construct(string $prefix)
    {
        $this->prefix = $prefix;
    }

    public function getExecutionClosure(): Closure
    {
        return function (string $value): string {
            return $this->prefix . $value;
        };
    }

    public function getReversionClosure(): Closure
    {
        return function (string $value): string {
            $length = strlen($this->prefix);
            if (substr($value, 0, $length) !== $this->prefix) {
                throw new UnexpectedPrefixException($this->prefix, $value);
            }
            return substr($value, $length);
        };
    }
}
