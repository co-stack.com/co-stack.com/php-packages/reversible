<?php

declare(strict_types=1);

namespace CoStack\Reversible\Operation\Encoding;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Exception\InvalidArgumentTypeException;
use CoStack\Reversible\Exception\JsonDecodeException;
use CoStack\Reversible\Exception\JsonEncodeException;

use function CoStack\Reversible\json_decode_assoc;
use function CoStack\Reversible\json_encode_strict;

/**
 * @method string execute($value)
 */
class JsonEncoding extends AbstractReversible
{
    public function getExecutionClosure(): Closure
    {
        return
            /**
             * @param array|scalar|object $value
             * @return string
             * @throws InvalidArgumentTypeException
             * @throws JsonEncodeException
             * @psalm-suppress DocblockTypeContradiction
             */
            static function ($value): string {
                return json_encode_strict($value);
            };
    }

    public function getReversionClosure(): Closure
    {
        return
            /**
             * @return array|bool|int|float|string|object
             * @throws JsonDecodeException
             */
            static function (string $value) {
                return json_decode_assoc($value);
            };
    }
}
