<?php

declare(strict_types=1);

namespace CoStack\Reversible\Operation\Encoding;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Exception\InvalidArgumentTypeException;
use CoStack\Reversible\TypeLossy;

use function base64_decode;
use function base64_encode;
use function gettype;
use function is_scalar;

/**
 * @method string execute($value)
 * @method string reverse($value)
 */
class Base64Encoding extends AbstractReversible implements TypeLossy
{
    public function getExecutionClosure(): Closure
    {
        return
            /**
             * @param scalar $value
             * @psalm-suppress DocblockTypeContradiction
             * @throws InvalidArgumentTypeException
             */
            static function ($value): string {
                if (!is_scalar($value)) {
                    throw new InvalidArgumentTypeException('value', 'scalar', gettype($value));
                }
                return base64_encode((string)$value);
            };
    }

    public function getReversionClosure(): Closure
    {
        return static function (string $value): string {
            return base64_decode($value, true);
        };
    }
}
