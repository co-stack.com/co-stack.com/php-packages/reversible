<?php

declare(strict_types=1);

namespace CoStack\Reversible\Operation\Encoding;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Exception\InvalidArgumentTypeException;
use CoStack\Reversible\Exception\UnserializeFailedException;

use function gettype;
use function is_array;
use function is_object;
use function is_scalar;
use function restore_error_handler;
use function serialize;
use function set_error_handler;
use function unserialize;

/**
 * @method string execute($value)
 */
class SerializationEncoding extends AbstractReversible
{
    /** @var array<array-key, string>|bool */
    private $allowedClasses;

    /**
     * @param array<array-key, string>|bool $allowedClasses Defaults to `false` because it is the most secure option.
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag) Inherited from unserialize
     */
    public function __construct($allowedClasses = false)
    {
        $this->allowedClasses = $allowedClasses;
    }

    public function getExecutionClosure(): Closure
    {
        return
            /**
             * @param array|scalar|object $value
             * @psalm-suppress DocblockTypeContradiction
             * @throws InvalidArgumentTypeException
             */
            static function ($value): string {
                if (!(is_array($value) || is_scalar($value) || is_object($value))) {
                    throw new InvalidArgumentTypeException('value', 'array|scalar|object', gettype($value));
                }
                return serialize($value);
            };
    }

    /**
     * @noinspection PhpRedundantVariableDocTypeInspection
     */
    public function getReversionClosure(): Closure
    {
        $allowedClasses = $this->allowedClasses;
        return
            /**
             * @return mixed
             * @throws UnserializeFailedException
             */
            static function (string $value) use ($allowedClasses) {
                set_error_handler(
                    static function (int $errorCode, string $errorMessage) use ($value): bool {
                        if (8 === $errorCode) {
                            restore_error_handler();
                            throw new UnserializeFailedException($errorCode, $errorMessage, $value);
                        }
                        // @codeCoverageIgnoreStart
                        return false;
                        // @codeCoverageIgnoreEnd
                    }
                );
                /** @var mixed $result */
                $result = unserialize($value, ['allowed_classes' => $allowedClasses]);
                restore_error_handler();
                return $result;
            };
    }
}
