<?php

declare(strict_types=1);

namespace CoStack\Reversible\Operation\Data;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Exception\InvalidArgumentException;
use CoStack\Reversible\Exception\JsonDecodeException;

use function CoStack\Reversible\json_decode_assoc;
use function count;
use function is_array;
use function json_encode;

class SpliceData extends AbstractReversible
{
    /** @var mixed */
    private $variable;

    /** @param mixed $variable */
    public function __construct(&$variable)
    {
        $this->variable = &$variable;
    }

    public function getExecutionClosure(): Closure
    {
        return /** @param scalar $string */ function ($string): string {
            return json_encode([$string, $this->variable]);
        };
    }

    public function getReversionClosure(): Closure
    {
        return
            /**
             * @param string $string
             * @return mixed
             * @throws InvalidArgumentException
             * @throws JsonDecodeException
             * @psalm-suppress MixedAssignment
             */
            function (string $string) {
                $decoded = json_decode_assoc($string);
                if (!is_array($decoded) || count($decoded) !== 2) {
                    throw new InvalidArgumentException('The string is formed incorrectly');
                }
                [$string, $this->variable] = $decoded;
                return $string;
            };
    }
}
