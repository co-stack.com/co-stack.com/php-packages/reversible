<?php

declare(strict_types=1);

namespace CoStack\Reversible\Applicable;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Reversible;

/**
 * @method array execute($value)
 * @method array reverse($value)
 */
class ApplyOnArrayValueByKey extends AbstractReversible
{
    /** @var Reversible */
    private $reversible;

    /** @var array<int, string> */
    private $keys;

    /** @param array<int, string> $keys */
    public function __construct(Reversible $reversible, array $keys)
    {
        $this->reversible = $reversible;
        $this->keys = $keys;
    }

    public function getExecutionClosure(): Closure
    {
        $executionClosure = $this->reversible->getExecutionClosure();
        return function (array $value) use ($executionClosure): array {
            foreach ($this->keys as $key) {
                if (!empty($value[$key])) {
                    /** @psalm-suppress MixedAssignment */
                    $value[$key] = $executionClosure($value[$key]);
                }
            }
            return $value;
        };
    }

    public function getReversionClosure(): Closure
    {
        $reversionClosure = $this->reversible->getReversionClosure();
        return function (array $value) use ($reversionClosure): array {
            foreach ($this->keys as $key) {
                if (!empty($value[$key])) {
                    /** @psalm-suppress MixedAssignment */
                    $value[$key] = $reversionClosure($value[$key]);
                }
            }
            return $value;
        };
    }
}
