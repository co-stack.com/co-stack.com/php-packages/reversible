<?php

declare(strict_types=1);

namespace CoStack\Reversible\Applicable;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Reversible;

use function array_reverse;

class ReversiblePipe extends AbstractReversible
{
    /** @var Reversible[] */
    private $functions = [];

    public function enqueue(Reversible $function): self
    {
        $this->functions[] = $function;
        return $this;
    }

    public function getExecutionClosure(): Closure
    {
        return
            /**
             * @param mixed $value
             * @return mixed
             */
            function ($value) {
                foreach ($this->functions as $function) {
                    /** @var mixed $value */
                    $value = $function->execute($value);
                }
                return $value;
            };
    }

    public function getReversionClosure(): Closure
    {
        return
            /**
             * @param mixed $value
             * @return mixed
             */
            function ($value) {
                foreach (array_reverse($this->functions) as $function) {
                    /** @var mixed $value */
                    $value = $function->reverse($value);
                }
                return $value;
            };
    }
}
