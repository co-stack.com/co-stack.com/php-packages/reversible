<?php

declare(strict_types=1);

namespace CoStack\Reversible\Applicable;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Reversible;

use function array_map;
use function is_array;

class ApplyOnArrayValueRecursively extends AbstractReversible
{
    /** @var Reversible */
    private $reversible;

    public function __construct(Reversible $reversible)
    {
        $this->reversible = $reversible;
    }

    /**
     * @psalm-suppress UnusedVariable
     * @noinspection PhpRedundantVariableDocTypeInspection
     */
    public function getExecutionClosure(): Closure
    {
        $executionClosure = $this->reversible->getExecutionClosure();
        return ($recursion = /** @param mixed $value */ static function ($value) use (&$recursion, $executionClosure) {
            if (is_array($value)) {
                /** @var Closure $recursion */
                return array_map($recursion, $value);
            }
            return $executionClosure($value);
        });
    }

    /**
     * @psalm-suppress UnusedVariable
     * @noinspection PhpRedundantVariableDocTypeInspection
     */
    public function getReversionClosure(): Closure
    {
        $reversionClosure = $this->reversible->getReversionClosure();
        return ($recursion = /** @param mixed $value */ static function ($value) use (&$recursion, $reversionClosure) {
            if (is_array($value)) {
                /** @var Closure $recursion */
                return array_map($recursion, $value);
            }
            return $reversionClosure($value);
        });
    }
}
