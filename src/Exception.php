<?php

declare(strict_types=1);

namespace CoStack\Reversible;

/**
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
class Exception extends \Exception
{
}
