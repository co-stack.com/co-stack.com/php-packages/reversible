<?php

declare(strict_types=1);

namespace CoStack\Reversible\Exception;

use CoStack\Reversible\Exception;
use Throwable;

class MissingPrivateKeyException extends Exception
{
    public const CODE = 1621360864;
    private const MESSAGE = 'You must set a private key if you want to decrypt a value';

    public function __construct(Throwable $previous = null)
    {
        parent::__construct(self::MESSAGE, self::CODE, $previous);
    }
}
