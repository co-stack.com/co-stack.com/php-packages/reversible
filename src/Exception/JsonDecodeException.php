<?php

declare(strict_types=1);

namespace CoStack\Reversible\Exception;

use CoStack\Reversible\Exception;
use Throwable;

use function sprintf;

class JsonDecodeException extends Exception
{
    public const CODE = 1621340544;
    private const MESSAGE = 'json_decode error [%d]: "%s"';

    /** @var int */
    private $errorCode;

    /** @var string */
    private $errorMessage;

    /** @var mixed */
    private $value;

    /** @param mixed $value */
    public function __construct(int $errorCode, string $errorMessage, $value, Throwable $previous = null)
    {
        $this->errorCode = $errorCode;
        $this->errorMessage = $errorMessage;
        $this->value = $value;
        parent::__construct(sprintf(self::MESSAGE, $errorCode, $errorMessage), self::CODE, $previous);
    }

    public function getErrorCode(): int
    {
        return $this->errorCode;
    }

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    /** @return mixed */
    public function getValue()
    {
        return $this->value;
    }
}
