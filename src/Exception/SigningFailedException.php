<?php

declare(strict_types=1);

namespace CoStack\Reversible\Exception;

use CoStack\Reversible\Exception;
use Throwable;

class SigningFailedException extends Exception
{
    public const CODE = 1622131620;
    private const MESSAGE = 'Creating the signature failed';

    /** @var array */
    private $errors;

    public function __construct(array $errors, Throwable $previous = null)
    {
        $this->errors = $errors;
        parent::__construct(self::MESSAGE, self::CODE, $previous);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
