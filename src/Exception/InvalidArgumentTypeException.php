<?php

declare(strict_types=1);

namespace CoStack\Reversible\Exception;

use CoStack\Reversible\Exception;
use Throwable;

use function sprintf;

class InvalidArgumentTypeException extends Exception
{
    public const CODE = 1581073983;
    private const MESSAGE = 'Argument "%s" is expected to be of type "%s", "%s" given';

    /** @var string */
    private $name;

    /** @var string */
    private $expected;

    /** @var string */
    private $actual;

    public function __construct(string $name, string $expected, string $actual, Throwable $previous = null)
    {
        $this->name = $name;
        $this->expected = $expected;
        $this->actual = $actual;
        parent::__construct(sprintf(self::MESSAGE, $name, $expected, $actual), self::CODE, $previous);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getExpected(): string
    {
        return $this->expected;
    }

    public function getActual(): string
    {
        return $this->actual;
    }
}
