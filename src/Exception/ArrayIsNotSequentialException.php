<?php

declare(strict_types=1);

namespace CoStack\Reversible\Exception;

use CoStack\Reversible\Exception;
use Throwable;

use function sprintf;

class ArrayIsNotSequentialException extends Exception
{
    public const CODE = 1581089760;
    private const MESSAGE = 'The array passed to "%s" is not sequential (it has associative keys).';

    /** @var string */
    private $method;

    public function __construct(string $method, Throwable $previous = null)
    {
        $this->method = $method;
        parent::__construct(sprintf(self::MESSAGE, $method), self::CODE, $previous);
    }

    public function getMethod(): string
    {
        return $this->method;
    }
}
