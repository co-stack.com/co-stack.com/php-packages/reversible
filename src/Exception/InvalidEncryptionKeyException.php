<?php

declare(strict_types=1);

namespace CoStack\Reversible\Exception;

use CoStack\Reversible\Exception;
use Throwable;

use function get_class;
use function gettype;
use function is_object;
use function sprintf;

class InvalidEncryptionKeyException extends Exception
{
    public const CODE = 1621358794;
    private const MESSAGE = 'The %s key is expected to be of type %s but is a "%s"';

    /** @var string */
    private $expected;

    /** @var string */
    private $usage;

    /** @var mixed */
    private $key;

    /** @param mixed $key */
    public function __construct(string $expected, string $usage, $key, Throwable $previous = null)
    {
        $this->expected = $expected;
        $this->usage = $usage;
        $this->key = $key;
        parent::__construct(
            sprintf(self::MESSAGE, $usage, $expected, is_object($key) ? get_class($key) : gettype($key)),
            self::CODE,
            $previous
        );
    }

    public function getExpected(): string
    {
        return $this->expected;
    }

    public function getUsage(): string
    {
        return $this->usage;
    }

    /** @return mixed */
    public function getKey()
    {
        return $this->key;
    }
}
