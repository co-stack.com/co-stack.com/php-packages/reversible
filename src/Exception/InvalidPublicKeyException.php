<?php

/**
 * @noinspection PhpElementIsNotAvailableInCurrentPhpVersionInspection
 */

declare(strict_types=1);

namespace CoStack\Reversible\Exception;

use CoStack\Reversible\Exception;
use OpenSSLAsymmetricKey;
use OpenSSLCertificate;
use Throwable;

class InvalidPublicKeyException extends Exception
{
    public const CODE = 1622125326;
    private const MESSAGE = 'The given string is not a valid public key';

    /** @var OpenSSLAsymmetricKey|OpenSSLCertificate|array|string|null */
    private $key;

    /** @var array */
    private $errors;

    /** @param OpenSSLAsymmetricKey|OpenSSLCertificate|array|string|null $key */
    public function __construct(array $errors, $key, Throwable $previous = null)
    {
        $this->key = $key;
        $this->errors = $errors;
        parent::__construct(self::MESSAGE, self::CODE, $previous);
    }

    /** @return OpenSSLAsymmetricKey|OpenSSLCertificate|array|string|null */
    public function getKey()
    {
        return $this->key;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
