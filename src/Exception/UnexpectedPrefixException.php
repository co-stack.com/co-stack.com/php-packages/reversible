<?php

declare(strict_types=1);

namespace CoStack\Reversible\Exception;

use CoStack\Reversible\Exception;
use Throwable;

use function sprintf;

class UnexpectedPrefixException extends Exception
{
    public const CODE = 1621342347;
    private const MESSAGE = 'The string "%s" was not prefixed with the expected prefix "%s"';

    /** @var string */
    private $prefix;

    /** @var string */
    private $string;

    public function __construct(string $prefix, string $string, Throwable $previous = null)
    {
        $this->prefix = $prefix;
        $this->string = $string;
        parent::__construct(sprintf(self::MESSAGE, $string, $prefix), self::CODE, $previous);
    }

    public function getPrefix(): string
    {
        return $this->prefix;
    }

    public function getString(): string
    {
        return $this->string;
    }
}
