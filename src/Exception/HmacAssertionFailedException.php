<?php

declare(strict_types=1);

namespace CoStack\Reversible\Exception;

use CoStack\Reversible\Exception;
use Throwable;

class HmacAssertionFailedException extends Exception
{
    public const CODE = 1605190985;
    private const MESSAGE = 'HMAC validation failed';

    public function __construct(Throwable $previous = null)
    {
        parent::__construct(self::MESSAGE, self::CODE, $previous);
    }
}
