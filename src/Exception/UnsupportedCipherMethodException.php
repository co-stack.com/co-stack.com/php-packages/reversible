<?php

declare(strict_types=1);

namespace CoStack\Reversible\Exception;

use CoStack\Reversible\Exception;
use Throwable;

use function sprintf;

class UnsupportedCipherMethodException extends Exception
{
    public const CODE = 1621349100;
    private const MESSAGE = 'The cypher algorithm "%s" is not in te list of supported algorithms.';

    /** @var string */
    private $algo;

    /** @var array */
    private $available;

    public function __construct(string $algo, array $available, Throwable $previous = null)
    {
        $this->algo = $algo;
        $this->available = $available;
        parent::__construct(sprintf(self::MESSAGE, $algo), self::CODE, $previous);
    }

    public function getAlgo(): string
    {
        return $this->algo;
    }

    public function getAvailable(): array
    {
        return $this->available;
    }
}
