<?php

declare(strict_types=1);

namespace CoStack\Reversible\Exception;

use CoStack\Reversible\Exception;
use Throwable;

use function implode;
use function sprintf;

use const PHP_EOL;

class DecryptionFailedException extends Exception
{
    public const CODE = 1621350377;
    private const MESSAGE = "The encrypted text could not be decrypted. Errors:\n%s";

    /** @var array<int|string> */
    private $errors;

    /** @param array<int|string> $errors */
    public function __construct(array $errors, Throwable $previous = null)
    {
        $this->errors = $errors;
        parent::__construct(sprintf(self::MESSAGE, implode(PHP_EOL, $errors)), self::CODE, $previous);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
