<?php

declare(strict_types=1);

namespace CoStack\Reversible\Exception;

use CoStack\Reversible\Exception;
use Throwable;

class MissingPublicKeyException extends Exception
{
    public const CODE = 1621360764;
    private const MESSAGE = 'You must set a public key if you want to encrypt a value';

    public function __construct(Throwable $previous = null)
    {
        parent::__construct(self::MESSAGE, self::CODE, $previous);
    }
}
