<?php

declare(strict_types=1);

namespace CoStack\Reversible\Exception;

use CoStack\Reversible\Exception;
use Throwable;

use function sprintf;

class UnserializeFailedException extends Exception
{
    public const CODE = 1631606863;
    private const MESSAGE = '%d: %s';

    /** @var int */
    private $errorCode;

    /** @var string */
    private $errorMessage;

    /** @var string */
    private $input;

    public function __construct(int $errorCode, string $errorMessage, string $input, Throwable $previous = null)
    {
        parent::__construct(sprintf(self::MESSAGE, $errorCode, $errorMessage), self::CODE, $previous);
        $this->errorCode = $errorCode;
        $this->errorMessage = $errorMessage;
        $this->input = $input;
    }

    public function getErrorCode(): int
    {
        return $this->errorCode;
    }

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function getInput(): string
    {
        return $this->input;
    }
}
