<?php

declare(strict_types=1);

namespace CoStack\Reversible\Exception;

use CoStack\Reversible\Exception;
use Throwable;

class EmptyStringException extends Exception
{
    public const CODE = 1636204185;
    private const MESSAGE = 'Argument %d passed to %s must be a non-empty string, an empty string was given';

    /** @var int */
    private $position;

    /** @var string */
    private $function;

    public function __construct(int $position, string $function, Throwable $previous = null)
    {
        $this->position = $position;
        $this->function = $function;
        parent::__construct(sprintf(self::MESSAGE, $position, $function), self::CODE, $previous);
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function getFunction(): string
    {
        return $this->function;
    }
}
