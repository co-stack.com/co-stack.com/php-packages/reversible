<?php

declare(strict_types=1);

namespace CoStack\Reversible\Exception;

use CoStack\Reversible\Exception;
use Throwable;

use function sprintf;

class RangeException extends Exception
{
    public const CODE = 1621598353;
    private const MESSAGE = 'The integer %d is not in the expected range from %d to %d';

    /** @var int */
    private $given;

    /** @var int */
    private $low;

    /** @var int */
    private $high;

    public function __construct(int $given, int $low, int $high, Throwable $previous = null)
    {
        $this->given = $given;
        $this->low = $low;
        $this->high = $high;
        parent::__construct(sprintf(self::MESSAGE, $given, $low, $high), self::CODE, $previous);
    }

    public function getGiven(): int
    {
        return $this->given;
    }

    public function getLow(): int
    {
        return $this->low;
    }

    public function getHigh(): int
    {
        return $this->high;
    }
}
