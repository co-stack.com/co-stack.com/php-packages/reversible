<?php

declare(strict_types=1);

namespace CoStack\Reversible\Exception;

use CoStack\Reversible\Exception;
use Throwable;

class InvalidArgumentException extends Exception
{
    public const CODE = 1627026950;
    private const MESSAGE = 'The given argument is invalid: %s';

    /** @var string */
    protected $reason;

    public function __construct(string $reason, Throwable $previous = null)
    {
        $this->reason = $reason;
        parent::__construct(sprintf(self::MESSAGE, $reason), self::CODE, $previous);
    }

    public function getReason(): string
    {
        return $this->reason;
    }
}
