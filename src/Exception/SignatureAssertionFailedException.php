<?php

declare(strict_types=1);

namespace CoStack\Reversible\Exception;

use CoStack\Reversible\Exception;
use Throwable;

class SignatureAssertionFailedException extends Exception
{
    public const CODE = 1622134627;
    private const MESSAGE = 'The signature is invalid';

    public function __construct(Throwable $previous = null)
    {
        parent::__construct(self::MESSAGE, self::CODE, $previous);
    }
}
