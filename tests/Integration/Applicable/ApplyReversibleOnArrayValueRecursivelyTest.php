<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Integration\Applicable;

use CoStack\Reversible\Applicable\ApplyOnArrayValueRecursively;
use CoStack\Reversible\Operation\Encoding\Base64Encoding;
use PHPUnit\Framework\TestCase;

use function base64_encode;

/**
 * @coversNothing
 * @SuppressWarnings(PHPMD.LongClassName)
 */
class ApplyReversibleOnArrayValueRecursivelyTest extends TestCase
{
    public function testBase64IsAppliedOnAllValues(): void
    {
        $input = [
            'key1' => [
                5 => 'value1',
            ],
            2 => 'value2',
            'key2' => [
                4 => [
                    'foo' => 'bar',
                ]
            ],
        ];

        $expected = [
            'key1' => [
                5 => base64_encode('value1'),
            ],
            2 => base64_encode('value2'),
            'key2' => [
                4 => [
                    'foo' => base64_encode('bar'),
                ]
            ],
        ];

        $applicable = new ApplyOnArrayValueRecursively(new Base64Encoding());
        $actual = $applicable->execute($input);

        $this->assertSame($expected, $actual);
    }
}
