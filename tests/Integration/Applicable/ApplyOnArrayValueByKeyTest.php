<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Integration\Applicable;

use CoStack\Reversible\Applicable\ApplyOnArrayValueByKey;
use CoStack\Reversible\Operation\Encoding\Base64Encoding;
use PHPUnit\Framework\TestCase;

use function base64_encode;

/**
 * @coversDefaultClass \CoStack\Reversible\Applicable\ApplyOnArrayValueByKey
 */
class ApplyOnArrayValueByKeyTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @uses \CoStack\Reversible\Operation\Encoding\Base64Encoding
     */
    public function testApplyOnArrayValueByKeyAppliesReversibleOnValuesAtExecution(): void
    {
        $input = [
            'key1' => 'foo',
            'key2' => 'bar',
            'key3' => 'baz',
        ];

        $expected = [
            'key1' => base64_encode('foo'),
            'key2' => 'bar',
            'key3' => base64_encode('baz'),
        ];

        $applicable = new ApplyOnArrayValueByKey(new Base64Encoding(), ['key1', 'key3', 'key4']);
        $actual = $applicable->execute($input);

        $this->assertSame($expected, $actual);
    }
    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\Operation\Encoding\Base64Encoding
     */
    public function testApplyOnArrayValueByKeyAppliesReversibleOnValuesAtReversion(): void
    {
        $input = [
            'key1' => base64_encode('foo'),
            'key2' => 'bar',
            'key3' => base64_encode('baz'),
        ];

        $expected = [
            'key1' => 'foo',
            'key2' => 'bar',
            'key3' => 'baz',
        ];

        $applicable = new ApplyOnArrayValueByKey(new Base64Encoding(), ['key1', 'key3', 'key4']);
        $actual = $applicable->reverse($input);

        $this->assertSame($expected, $actual);
    }
}
