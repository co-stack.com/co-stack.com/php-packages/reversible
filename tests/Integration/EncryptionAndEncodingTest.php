<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Integration;

use CoStack\Reversible\Applicable\ReversiblePipe;
use CoStack\Reversible\Operation\Encoding\Base64Encoding;
use CoStack\Reversible\Operation\Encryption\AsymmetricEncryption;
use PHPUnit\Framework\TestCase;

use function openssl_pkey_export;
use function openssl_pkey_get_details;
use function openssl_pkey_new;
use function random_bytes;

/**
 * @coversNothing
 */
class EncryptionAndEncodingTest extends TestCase
{
    public function testAsymmetricEncryptionAndBase64EncodingCanBeChained(): void
    {
        $privateKey = openssl_pkey_new();
        $publicKeyPem = openssl_pkey_get_details($privateKey)['key'];
        openssl_pkey_export($privateKey, $privateKeyPem);

        $asymmetricEncryption = new AsymmetricEncryption($privateKeyPem, $publicKeyPem);

        $pipe = new ReversiblePipe();
        $pipe->enqueue($asymmetricEncryption);
        $pipe->enqueue(new Base64Encoding());

        $expected = random_bytes(64);

        $intermediate = $pipe->execute($expected);
        $actual = $pipe->reverse($intermediate);

        $this->assertSame($expected, $actual);
    }
}
