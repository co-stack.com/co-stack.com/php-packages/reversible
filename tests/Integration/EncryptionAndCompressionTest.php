<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Integration;

use CoStack\Reversible\Applicable\ReversiblePipe;
use CoStack\Reversible\Operation\Compression\GzipCompression;
use CoStack\Reversible\Operation\Encryption\AsymmetricEncryption;
use PHPUnit\Framework\TestCase;

use function openssl_pkey_export;
use function openssl_pkey_get_details;
use function openssl_pkey_new;
use function random_bytes;

/**
 * @coversNothing
 */
class EncryptionAndCompressionTest extends TestCase
{
    public function testGzipCompressionAndAsymmetricEncryptionCanBeChained(): void
    {
        $privateKey = openssl_pkey_new();
        $publicKeyPem = openssl_pkey_get_details($privateKey)['key'];
        openssl_pkey_export($privateKey, $privateKeyPem);

        $asymmetricEncryption = new AsymmetricEncryption($privateKeyPem, $publicKeyPem);

        $pipe = new ReversiblePipe();
        $pipe->enqueue($asymmetricEncryption);
        $pipe->enqueue(new GzipCompression());

        $expected = random_bytes(64);

        $intermediate = $pipe->execute($expected);
        $actual = $pipe->reverse($intermediate);

        $this->assertSame($expected, $actual);
    }
}
