<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Applicable;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Applicable\ApplyOnArrayValueRecursively;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Applicable\ApplyOnArrayValueRecursively
 */
class ApplyOnArrayValueRecursivelyTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     */
    public function testApplyOnArrayValueByKeyExecutesReversibleForEachGivenArrayKey(): void
    {
        $checks = [
            'executed' => 0,
            'reversed' => 0,
        ];
        $seenValues = [];

        $reversible = new class ($checks, $seenValues) extends AbstractReversible {
            protected $checks;

            protected $seenValues;

            public function __construct(&$checks, &$seenValues)
            {
                $this->checks = &$checks;
                $this->seenValues = &$seenValues;
            }

            public function getExecutionClosure(): Closure
            {
                return function ($value): void {
                    $this->checks['executed']++;
                    $this->seenValues[] = $value;
                };
            }

            public function getReversionClosure(): Closure
            {
                return function ($value): void {
                    $this->checks['reversed']++;
                    $this->seenValues[] = $value;
                };
            }
        };

        $value = [
            'foo' => 'whoo',
            'bar' => [
                'beng' => 'boi',
                'feng' => [
                    'blubb' => 1,
                ],
            ],
            'baz' => 'whee',
        ];

        $applicable = new ApplyOnArrayValueRecursively($reversible);
        $applicable->execute($value);

        $this->assertSame(4, $checks['executed']);
        $this->assertSame(0, $checks['reversed']);
        $this->assertSame(['whoo', 'boi', 1, 'whee'], $seenValues);
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testApplyOnArrayValueByKeyReversesReversibleForEachGivenArrayKey(): void
    {
        $checks = [
            'executed' => 0,
            'reversed' => 0,
        ];
        $seenValues = [];

        $reversible = new class ($checks, $seenValues) extends AbstractReversible {
            protected $checks;

            protected $seenValues;

            public function __construct(&$checks, &$seenValues)
            {
                $this->checks = &$checks;
                $this->seenValues = &$seenValues;
            }

            public function getExecutionClosure(): Closure
            {
                return function ($value): void {
                    $this->checks['executed']++;
                    $this->seenValues[] = $value;
                };
            }

            public function getReversionClosure(): Closure
            {
                return function ($value): void {
                    $this->checks['reversed']++;
                    $this->seenValues[] = $value;
                };
            }
        };

        $value = [
            'foo' => 'whoo',
            'bar' => [
                'beng' => 'boi',
                'feng' => [
                    'blubb' => 1,
                ],
            ],
            'baz' => 'whee',
        ];

        $applicable = new ApplyOnArrayValueRecursively($reversible);
        $applicable->reverse($value);

        $this->assertSame(0, $checks['executed']);
        $this->assertSame(4, $checks['reversed']);
        $this->assertSame(['whoo', 'boi', 1, 'whee'], $seenValues);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     */
    public function testApplyOnArrayValueRecursivelyExecutionReturnsExpectedValue(): void
    {
        $input = [
            0 => [
                'ab',
                'cd',
            ],
            1 => 'ef',
        ];

        $expected = [
            0 => [
                'ab_executed',
                'cd_executed',
            ],
            1 => 'ef_executed',
        ];

        $reversible = new class extends AbstractReversible {
            public function getExecutionClosure(): Closure
            {
                return function ($value): string {
                    return $value . '_executed';
                };
            }

            public function getReversionClosure(): Closure
            {
                return function ($value): string {
                    return $value . '_reversed';
                };
            }
        };


        $applicable = new ApplyOnArrayValueRecursively($reversible);
        $actual = $applicable->execute($input);

        $this->assertSame($expected, $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testApplyOnArrayValueRecursivelyReversionReturnsExpectedValue(): void
    {
        $input = [
            0 => [
                'ab',
                'cd',
            ],
            1 => 'ef',
        ];

        $expected = [
            0 => [
                'ab_reversed',
                'cd_reversed',
            ],
            1 => 'ef_reversed',
        ];

        $reversible = new class extends AbstractReversible {
            public function getExecutionClosure(): Closure
            {
                return function ($value): string {
                    return $value . '_executed';
                };
            }

            public function getReversionClosure(): Closure
            {
                return function ($value): string {
                    return $value . '_reversed';
                };
            }
        };


        $applicable = new ApplyOnArrayValueRecursively($reversible);
        $actual = $applicable->reverse($input);

        $this->assertSame($expected, $actual);
    }
}
