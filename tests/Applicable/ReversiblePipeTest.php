<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Applicable;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Applicable\ReversiblePipe;
use CoStack\Reversible\Reversible;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Applicable\ReversiblePipe
 */
class ReversiblePipeTest extends TestCase
{
    /**
     * @covers ::enqueue
     * @covers ::execute
     * @covers ::getExecutionClosure
     */
    public function testReversiblePipeCallsAllEnqueuedExecutionsInOrder(): void
    {
        $called = [
            'executed' => [],
            'reversed' => [],
        ];

        $reversiblePipe = new ReversiblePipe();
        $reversiblePipe->enqueue($this->getReversible(1, $called));
        $reversiblePipe->enqueue($this->getReversible(2, $called));
        $reversiblePipe->enqueue($this->getReversible(3, $called));
        $reversiblePipe->execute(true);

        $this->assertSame([1, 2, 3], $called['executed']);
        $this->assertSame([], $called['reversed']);
    }
    /**
     * @covers ::enqueue
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testReversiblePipeCallsAllEnqueuedReversionsInOrder(): void
    {
        $called = [
            'executed' => [],
            'reversed' => [],
        ];

        $reversiblePipe = new ReversiblePipe();
        $reversiblePipe->enqueue($this->getReversible(1, $called));
        $reversiblePipe->enqueue($this->getReversible(2, $called));
        $reversiblePipe->enqueue($this->getReversible(3, $called));
        $reversiblePipe->reverse(true);

        $this->assertSame([], $called['executed']);
        $this->assertSame([3, 2, 1], $called['reversed']);
    }

    private function getReversible(int $identifier, array &$called): Reversible
    {
        return new class ($identifier, $called) extends AbstractReversible {
            protected $identifier;

            protected $called;

            public function __construct($identifier, &$called)
            {
                $this->identifier = $identifier;
                $this->called = &$called;
            }

            public function getExecutionClosure(): Closure
            {
                return function (): void {
                    $this->called['executed'][] = $this->identifier;
                };
            }

            public function getReversionClosure(): Closure
            {
                return function (): void {
                    $this->called['reversed'][] = $this->identifier;
                };
            }
        };
    }
}
