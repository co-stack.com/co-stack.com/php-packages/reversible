<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Applicable;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Applicable\ApplyOnArrayValueByKey;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Applicable\ApplyOnArrayValueByKey
 */
class ApplyOnArrayValueByKeyTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     */
    public function testApplyOnArrayValueByKeyExecutesReversibleForEachGivenArrayKey(): void
    {
        $checks = [
            'executed' => 0,
            'reversed' => 0,
        ];
        $seenValues = [];

        $reversible = new class ($checks, $seenValues) extends AbstractReversible  {
            protected $checks;

            protected $seenValues;

            public function __construct(&$checks, &$seenValues)
            {
                $this->checks = &$checks;
                $this->seenValues = &$seenValues;
            }

            public function getExecutionClosure(): Closure
            {
                return function (string $string): void {
                    $this->checks['executed']++;
                    $this->seenValues[] = $string;
                };
            }

            public function getReversionClosure(): Closure
            {
                return function (string $string): void {
                    $this->checks['reversed']++;
                    $this->seenValues[] = $string;
                };
            }
        };

        $value = [
            'foo' => 'whoo',
            'bar' => 'whaa',
            'baz' => 'whee',
        ];

        $applicable = new ApplyOnArrayValueByKey($reversible, ['foo', 'bar']);
        $applicable->execute($value);

        $this->assertSame(2, $checks['executed']);
        $this->assertSame(0, $checks['reversed']);
        $this->assertSame(['whoo', 'whaa'], $seenValues);
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testApplyOnArrayValueByKeyReversesReversibleForEachGivenArrayKey(): void
    {
        $checks = [
            'executed' => 0,
            'reversed' => 0,
        ];
        $seenValues = [];

        $reversible = new class ($checks, $seenValues) extends AbstractReversible {
            protected $checks;

            protected $seenValues;

            public function __construct(&$checks, &$seenValues)
            {
                $this->checks = &$checks;
                $this->seenValues = &$seenValues;
            }

            public function getExecutionClosure(): Closure
            {
                return function (string $string): void {
                    $this->checks['executed']++;
                    $this->seenValues[] = $string;
                };
            }

            public function getReversionClosure(): Closure
            {
                return function (string $string): void {
                    $this->checks['reversed']++;
                    $this->seenValues[] = $string;
                };
            }
        };

        $value = [
            'foo' => 'whoo',
            'bar' => 'whaa',
            'baz' => 'whee',
        ];

        $applicable = new ApplyOnArrayValueByKey($reversible, ['foo', 'bar']);
        $applicable->reverse($value);

        $this->assertSame(0, $checks['executed']);
        $this->assertSame(2, $checks['reversed']);
        $this->assertSame(['whoo', 'whaa'], $seenValues);
    }
}
