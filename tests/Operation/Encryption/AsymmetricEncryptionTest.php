<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Operation\Encryption;

use CoStack\Reversible\Exception\DecryptionFailedException;
use CoStack\Reversible\Exception\InvalidEncryptionKeyException;
use CoStack\Reversible\Exception\InvalidPrivateKeyException;
use CoStack\Reversible\Exception\InvalidPublicKeyException;
use CoStack\Reversible\Exception\MissingPrivateKeyException;
use CoStack\Reversible\Exception\MissingPublicKeyException;
use CoStack\Reversible\Operation\Encryption\AsymmetricEncryption;
use PHPUnit\Framework\TestCase;
use PHPUnit\Util\ErrorHandler;
use stdClass;

use function openssl_pkey_export;
use function openssl_pkey_get_details;
use function openssl_pkey_new;
use function set_error_handler;

/**
 * @coversDefaultClass \CoStack\Reversible\Operation\Encryption\AsymmetricEncryption
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class AsymmetricEncryptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::assertValidEncryptionKey
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @covers ::collectOpenSslErrors
     */
    public function testAsymmetricEncryptionEncryptsString(): void
    {
        $value = 'My secret value';

        $privateKey = openssl_pkey_new();
        $publicKeyPem = openssl_pkey_get_details($privateKey)['key'];
        openssl_pkey_export($privateKey, $privateKeyPem);

        $asymmetricEncryption = new AsymmetricEncryption($privateKeyPem, $publicKeyPem);
        $intermediate = $asymmetricEncryption->execute($value);

        $actual = $asymmetricEncryption->reverse($intermediate);

        $this->assertSame($value, $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::assertValidEncryptionKey
     * @uses \CoStack\Reversible\Exception\InvalidEncryptionKeyException::__construct
     */
    public function testAsymmetricEncryptionThrowsExceptionIfPrivateKeyTypeIsInvalid(): void
    {
        $this->expectException(InvalidEncryptionKeyException::class);

        // @phpstan-ignore-next-line
        new AsymmetricEncryption(new stdClass());
    }

    /**
     * @covers ::__construct
     * @covers ::assertValidEncryptionKey
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @covers ::collectOpenSslErrors
     * @uses \CoStack\Reversible\Exception\InvalidPrivateKeyException::__construct
     */
    public function testAsymmetricEncryptionThrowsExceptionIfPrivateKeyIsInvalid(): void
    {
        $this->expectException(InvalidPrivateKeyException::class);

        $asymmetricEncryption = new AsymmetricEncryption('not a valid private key');
        $asymmetricEncryption->reverse('string');
    }

    /**
     * @covers ::__construct
     * @covers ::assertValidEncryptionKey
     * @uses \CoStack\Reversible\Exception\InvalidEncryptionKeyException::__construct
     */
    public function testAsymmetricEncryptionThrowsExceptionIfPublicKeyTypeIsInvalid(): void
    {
        $this->expectException(InvalidEncryptionKeyException::class);

        // @phpstan-ignore-next-line
        new AsymmetricEncryption(null, new stdClass());
    }

    /**
     * @covers ::__construct
     * @covers ::assertValidEncryptionKey
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::collectOpenSslErrors
     * @uses \CoStack\Reversible\Exception\InvalidPublicKeyException::__construct
     */
    public function testAsymmetricEncryptionThrowsExceptionIfPublicKeyIsInvalid(): void
    {
        $this->expectException(InvalidPublicKeyException::class);

        $asymmetricEncryption = new AsymmetricEncryption(null, 'not a valid public key');
        $asymmetricEncryption->execute('value');
    }

    /**
     * @covers ::__construct
     * @covers ::assertValidEncryptionKey
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @covers ::collectOpenSslErrors
     * @uses \CoStack\Reversible\Exception\DecryptionFailedException::__construct
     * @uses \CoStack\Reversible\AbstractReversible::execute
     * @uses \CoStack\Reversible\Operation\Encryption\AsymmetricEncryption::getExecutionClosure
     */
    public function testAsymmetricEncryptionThrowsExceptionIfPrivateKeyIsWrong(): void
    {
        $privateKeyForExec = openssl_pkey_new();
        $publicKeyPem = openssl_pkey_get_details($privateKeyForExec)['key'];
        $asymmetricEncryption = new AsymmetricEncryption(null, $publicKeyPem);
        $encrypted = $asymmetricEncryption->execute('foo');

        $privateKey = openssl_pkey_new();
        openssl_pkey_export($privateKey, $privateKeyPem);
        $asymmetricEncryption = new AsymmetricEncryption($privateKeyPem);

        $this->expectException(DecryptionFailedException::class);

        $asymmetricEncryption->reverse($encrypted);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::assertValidEncryptionKey
     * @uses \CoStack\Reversible\Exception\MissingPublicKeyException::__construct
     */
    public function testAsymmetricEncryptionThrowsExceptionIfPublicKeyForEncryptionIsMissing(): void
    {
        $this->expectException(MissingPublicKeyException::class);

        $asymmetricEncryption = new AsymmetricEncryption();
        $asymmetricEncryption->execute('string');
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @covers ::assertValidEncryptionKey
     * @uses \CoStack\Reversible\Exception\MissingPrivateKeyException::__construct
     */
    public function testAsymmetricEncryptionThrowsExceptionIfPrivateKeyForDecryptionIsMissing(): void
    {
        $this->expectException(MissingPrivateKeyException::class);

        $asymmetricEncryption = new AsymmetricEncryption();
        $asymmetricEncryption->reverse('string');
    }

    /**
     * @covers ::__construct
     * @covers ::assertValidEncryptionKey
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @covers ::collectOpenSslErrors
     */
    public function testAsymmetricEncryptionOperationsFlushOpenSslErrors(): void
    {
        $privateKey = openssl_pkey_new();
        openssl_pkey_export($privateKey, $privateKeyPem);
        $publicKeyPem = openssl_pkey_get_details($privateKey)['key'];

        $asymmetricEncryption = new AsymmetricEncryption($privateKeyPem, $publicKeyPem);

        $intermediate = $asymmetricEncryption->execute('string');

        $this->assertFalse(openssl_error_string());

        $asymmetricEncryption->reverse($intermediate);

        $this->assertFalse(openssl_error_string());
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::collectOpenSslErrors
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @covers ::assertValidEncryptionKey
     * @uses \CoStack\Reversible\json_decode_assoc
     * @uses \CoStack\Reversible\json_encode_strict
     */
    public function testAsymmetricEncryptionExecutionRemovesTemporaryErrorHandlerOnSuccess(): void
    {
        $this->assertCurrentErrorHandlerIsPhpUnitOfDefault();

        $privateKey = openssl_pkey_new();
        openssl_pkey_export($privateKey, $privateKeyPem);
        $publicKeyPem = openssl_pkey_get_details($privateKey)['key'];
        $asymmetricEncryption = new AsymmetricEncryption($privateKeyPem, $publicKeyPem);

        $intermediate = $asymmetricEncryption->execute('some string');

        $this->assertCurrentErrorHandlerIsPhpUnitOfDefault();

        $asymmetricEncryption->reverse($intermediate);

        $this->assertCurrentErrorHandlerIsPhpUnitOfDefault();
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::collectOpenSslErrors
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @covers ::assertValidEncryptionKey
     * @uses \CoStack\Reversible\json_decode_assoc
     * @uses \CoStack\Reversible\json_encode_strict
     * @uses \CoStack\Reversible\Exception\InvalidPublicKeyException::__construct
     */
    public function testAsymmetricEncryptionExecutionRemovesTemporaryErrorHandlerOnFailure(): void
    {
        $this->assertCurrentErrorHandlerIsPhpUnitOfDefault();

        $asymmetricEncryption = new AsymmetricEncryption(null, 'not a valid public key');
        try {
            $asymmetricEncryption->execute('value');
        } /** @noinspection PhpRedundantCatchClauseInspection */ catch (InvalidPublicKeyException $exception) {
            // Discard exception
        }

        $this->assertCurrentErrorHandlerIsPhpUnitOfDefault();
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::collectOpenSslErrors
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @covers ::assertValidEncryptionKey
     * @uses \CoStack\Reversible\json_decode_assoc
     * @uses \CoStack\Reversible\json_encode_strict
     * @uses \CoStack\Reversible\Exception\InvalidPrivateKeyException::__construct
     */
    public function testAsymmetricEncryptionReversionRemovesTemporaryErrorHandlerOnFailure(): void
    {
        $this->assertCurrentErrorHandlerIsPhpUnitOfDefault();

        $asymmetricEncryption = new AsymmetricEncryption('not a valid private key');
        try {
            $asymmetricEncryption->reverse('string');
        } /** @noinspection PhpRedundantCatchClauseInspection */ catch (InvalidPrivateKeyException $exception) {
            // Discard exception
        }

        $this->assertCurrentErrorHandlerIsPhpUnitOfDefault();
    }

    protected function assertCurrentErrorHandlerIsPhpUnitOfDefault(): void
    {
        $firstErrorHandler = set_error_handler(null);
        $this->assertInstanceOf(ErrorHandler::class, $firstErrorHandler);
        $secondErrorHandler = set_error_handler(null);
        $this->assertNull($secondErrorHandler, 'The current error handler is unexpected');
        // restore original error handler
        set_error_handler($firstErrorHandler);
    }
}
