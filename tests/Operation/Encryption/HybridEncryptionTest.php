<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Operation\Encryption;

use CoStack\Reversible\Exception\DecryptionFailedException;
use CoStack\Reversible\Operation\Encryption\AsymmetricEncryption;
use CoStack\Reversible\Operation\Encryption\HybridEncryption;
use PHPUnit\Framework\TestCase;

use function json_encode;

/**
 * @coversDefaultClass \CoStack\Reversible\Operation\Encryption\HybridEncryption
 */
class HybridEncryptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\Operation\Encryption\AsymmetricEncryption::collectOpenSslErrors
     * @uses \CoStack\Reversible\Operation\Encryption\AsymmetricEncryption::__construct
     * @uses \CoStack\Reversible\Operation\Encryption\AsymmetricEncryption::assertValidEncryptionKey
     * @uses \CoStack\Reversible\Operation\Encryption\AsymmetricEncryption::getExecutionClosure
     * @uses \CoStack\Reversible\Operation\Encryption\AsymmetricEncryption::getReversionClosure
     * @uses \CoStack\Reversible\Operation\Encryption\SymmetricEncryption::__construct
     * @uses \CoStack\Reversible\Operation\Encryption\SymmetricEncryption::getExecutionClosure
     * @uses \CoStack\Reversible\Operation\Encryption\SymmetricEncryption::getReversionClosure
     * @uses \CoStack\Reversible\json_decode_assoc
     * @uses \CoStack\Reversible\json_encode_strict
     */
    public function testHybridEncryptionWorks(): void
    {
        $privateKey = openssl_pkey_new();
        $publicKeyPem = openssl_pkey_get_details($privateKey)['key'];
        openssl_pkey_export($privateKey, $privateKeyPem);

        $asymmetricEncryption = new  AsymmetricEncryption($privateKeyPem, $publicKeyPem);

        $canary = uniqid('reversible_canary_', true);

        $operationWithSecret = new HybridEncryption($asymmetricEncryption);

        $encrypted = $operationWithSecret->execute($canary);

        $hybridEncryption = new HybridEncryption($asymmetricEncryption);

        $decrypted = $hybridEncryption->reverse($encrypted);

        $this->assertSame($canary, $decrypted);
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\json_decode_assoc
     * @uses \CoStack\Reversible\Operation\Encryption\AsymmetricEncryption
     * @uses \CoStack\Reversible\Exception\DecryptionFailedException::__construct
     */
    public function testHybridEncryptionThrowsExceptionIfInputIsNotAnArray(): void
    {
        $input = json_encode(1);

        $asymmetricEncryption = new  AsymmetricEncryption();
        $operationWithSecret = new HybridEncryption($asymmetricEncryption);

        $this->expectException(DecryptionFailedException::class);
        $this->expectExceptionMessage('The input value is invalid');

        $operationWithSecret->reverse($input);
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\json_decode_assoc
     * @uses \CoStack\Reversible\Operation\Encryption\AsymmetricEncryption
     * @uses \CoStack\Reversible\Exception\DecryptionFailedException::__construct
     */
    public function testHybridEncryptionThrowsExceptionIfInputArrayDoesNotContainStringData(): void
    {
        $input = json_encode([1, 'string']);

        $asymmetricEncryption = new  AsymmetricEncryption();
        $operationWithSecret = new HybridEncryption($asymmetricEncryption);

        $this->expectException(DecryptionFailedException::class);
        $this->expectExceptionMessage('The input value is invalid');

        $operationWithSecret->reverse($input);
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\json_decode_assoc
     * @uses \CoStack\Reversible\Operation\Encryption\AsymmetricEncryption
     * @uses \CoStack\Reversible\Exception\DecryptionFailedException::__construct
     */
    public function testHybridEncryptionThrowsExceptionIfInputArrayDoesNotContainStringSecret(): void
    {
        $input = json_encode(['string', 1]);

        $asymmetricEncryption = new  AsymmetricEncryption();
        $operationWithSecret = new HybridEncryption($asymmetricEncryption);

        $this->expectException(DecryptionFailedException::class);
        $this->expectExceptionMessage('The input value is invalid');

        $operationWithSecret->reverse($input);
    }
}
