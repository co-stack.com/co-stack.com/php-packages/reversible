<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Operation\Encryption;

use CoStack\Reversible\Exception\DecryptionFailedException;
use CoStack\Reversible\Exception\EmptyStringException;
use CoStack\Reversible\Exception\UnsupportedCipherMethodException;
use CoStack\Reversible\Operation\Encryption\SymmetricEncryption;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Operation\Encryption\SymmetricEncryption
 */
class SymmetricEncryptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricEncryptionCanEncryptEndDecryptString(): void
    {
        $value = 'My super secret string';

        $symmetricEncryption = new SymmetricEncryption('My super secret encryption key', 'aes-128-gcm', '`');
        $intermediate = $symmetricEncryption->execute($value);

        $this->assertStringNotContainsStringIgnoringCase($value, $intermediate);
        $this->assertStringContainsStringIgnoringCase('`', $intermediate);

        $actual = $symmetricEncryption->reverse($intermediate);

        $this->assertSame($value, $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\Exception\DecryptionFailedException::__construct
     * @uses \CoStack\Reversible\Operation\OpenSSL::collectOpenSslErrors
     */
    public function testSymmetricEncryptionThrowsExceptionIfDecryptionSecretIsWrong(): void
    {
        $value = 'My super secret string';

        $symmetricEncryption = new SymmetricEncryption('My super secret encryption key');
        $intermediate = $symmetricEncryption->execute($value);

        $this->assertStringNotContainsStringIgnoringCase($value, $intermediate);

        $this->expectException(DecryptionFailedException::class);

        $symmetricEncryption = new SymmetricEncryption('My super wrong encryption key');
        $symmetricEncryption->reverse($intermediate);
    }

    /**
     * @covers ::__construct
     * @uses \CoStack\Reversible\Exception\UnsupportedCipherMethodException::__construct
     */
    public function testSymmetricEncryptionThrowsExceptionIfAlgorithmIsUnsupported(): void
    {
        $this->expectException(UnsupportedCipherMethodException::class);

        new SymmetricEncryption('My super secret encryption key', 'does not exist');
    }

    /**
     * @covers ::__construct
     * @uses \CoStack\Reversible\Exception\EmptyStringException::__construct::__construct
     */
    public function testSymmetricEncryptionThrowsExceptionIfGlueIsEmpty(): void
    {
        $this->expectException(EmptyStringException::class);
        $this->expectExceptionMessage('Argument 0 passed to CoStack\Reversible\Operation\Encryption\SymmetricEncryption::__construct must be a non-empty string, an empty string was given');

        // @phpstan-ignore-next-line
        new SymmetricEncryption('My super secret encryption key', 'aes-256-gcm', '');
    }
}
