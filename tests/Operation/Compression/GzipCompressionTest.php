<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Operation\Compression;

use CoStack\Reversible\Exception\RangeException;
use CoStack\Reversible\Operation\Compression\GzipCompression;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Operation\Compression\GzipCompression
 */
class GzipCompressionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testGzipCompressionCompressesString(): void
    {
        $string = str_repeat('foo_barbaz_boo', 100);

        $gzipCompression = new GzipCompression();
        $compressedString = $gzipCompression->execute($string);

        $this->assertTrue(strlen($compressedString) < strlen($string));

        $gzipCompression = new GzipCompression();
        $actual = $gzipCompression->reverse($compressedString);

        $this->assertSame($string, $actual);
    }

    /**
     * @covers ::__construct
     * @uses \CoStack\Reversible\Exception\RangeException::__construct
     */
    public function testGzipCompressionThrowsExceptionForInvalidBlocksize(): void
    {
        $this->expectException(RangeException::class);
        $this->expectExceptionMessage('The integer 10 is not in the expected range from 1 to 9');

        new GzipCompression(10);
    }

    public function blocksizesDataProvider(): array
    {
        return [
            [0],
            [1],
            [2],
            [3],
            [4],
            [5],
            [6],
            [7],
            [8],
            [9],
        ];
    }

    /**
     * @covers ::__construct
     * @uses         \CoStack\Reversible\Exception\RangeException::__construct
     * @dataProvider blocksizesDataProvider
     */
    public function testGzipCompressionAcceptsValidBlocksizes(int $blocksize): void
    {
        new GzipCompression($blocksize);

        // Assert that no exception will be thrown
        $this->assertTrue(true);
    }
}
