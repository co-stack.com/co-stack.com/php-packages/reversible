<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Operation\Mapping;

use CoStack\Reversible\Operation\Mapping\ArrayKeyMapping;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Operation\Mapping\ArrayKeyMapping
 */
class ArrayKeyMappingTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     */
    public function testArrayKeyMappingReplacesAssociativeKeysWithInteger(): void
    {
        $mapping = [
            0 => 'foo',
            1 => 'bar',
            2 => 'baz',
        ];

        $value = [
            'baz' => 'whee',
            4 => 'whoo',
            'foo' => 'whaa',
        ];

        $expected = [
            0 => 'whaa',
            2 => 'whee',
            4 => 'whoo',
        ];

        $arrayKeyMapping = new ArrayKeyMapping($mapping);
        $actual = $arrayKeyMapping->execute($value);

        $this->assertSame($expected, $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testArrayKeyMappingReplacesMappedIntegerKeyWithAssociativeKeys(): void
    {
        $mapping = [
            0 => 'foo',
            1 => 'bar',
            2 => 'baz',
        ];

        $value = [
            2 => 'whee',
            4 => 'whoo',
            0 => 'whaa',
        ];

        $expected = [
            'baz' => 'whee',
            4 => 'whoo',
            'foo' => 'whaa',
        ];

        $arrayKeyMapping = new ArrayKeyMapping($mapping);
        $actual = $arrayKeyMapping->reverse($value);

        $this->assertSame($expected, $actual);
    }
}
