<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Operation\Fixed;

use CoStack\Reversible\Exception\UnexpectedPrefixException;
use CoStack\Reversible\Operation\Fixed\FixedPrefix;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Operation\Fixed\FixedPrefix
 */
class FixedPrefixTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     */
    public function testFixedPrefixExecutionAddsStringAtBeginning(): void
    {
        $fixedPrefix = new FixedPrefix('foo');
        $actual = $fixedPrefix->execute('bar');

        $this->assertSame('foobar', $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testFixedPrefixReversionRemovesStringFromBeginning(): void
    {
        $fixedPrefix = new FixedPrefix('foo');
        $actual = $fixedPrefix->reverse('foobar');

        $this->assertSame('bar', $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\Exception\UnexpectedPrefixException::__construct
     */
    public function testFixedPrefixReversionThrowsExceptionIfStringPrefixWasUnexpected(): void
    {
        $this->expectException(UnexpectedPrefixException::class);

        $fixedPrefix = new FixedPrefix('foo');
        $fixedPrefix->reverse('bazbar');
    }
}
