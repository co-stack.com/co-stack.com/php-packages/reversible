<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Operation\Fixed;

use CoStack\Reversible\Operation\Fixed\FixedStringStripping;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Operation\Fixed\FixedStringStripping
 */
class FixedStringStrippingTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     */
    public function testFixedStringStrippingRemovesStringsAtGivenPositions(): void
    {
        $fixedStringStripping = new FixedStringStripping('foo', [2, 7, 10]);
        $actual = $fixedStringStripping->execute('hello world, this is bar');

        $this->assertSame('he world,is is bar', $actual);
    }
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     */
    public function testFixedStringStrippingRemovesNothingIfPositionIsGreaterThanStringLength(): void
    {
        $fixedStringStripping = new FixedStringStripping('foo', [567]);
        $actual = $fixedStringStripping->execute('hello world, this is bar');

        $this->assertSame('hello world, this is bar', $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testFixedStringStrippingAddsStringsAtGivenPositions(): void
    {
        $fixedStringStripping = new FixedStringStripping('foo', [2, 7, 10]);
        $actual = $fixedStringStripping->reverse('he world,is is bar');

        $this->assertSame('hefoo world,foois is barfoo', $actual);
    }
}
