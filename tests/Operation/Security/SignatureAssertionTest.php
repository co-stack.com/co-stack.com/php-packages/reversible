<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Operation\Security;

use CoStack\Reversible\Exception\InvalidEncryptionKeyException;
use CoStack\Reversible\Exception\InvalidPrivateKeyException;
use CoStack\Reversible\Exception\InvalidPublicKeyException;
use CoStack\Reversible\Exception\SignatureAssertionFailedException;
use CoStack\Reversible\Operation\Security\SignatureAssertion;
use PHPUnit\Framework\TestCase;
use PHPUnit\Util\ErrorHandler;
use stdClass;

use function base64_decode;
use function base64_encode;
use function json_decode;
use function json_encode;
use function openssl_pkey_export;
use function openssl_pkey_get_details;
use function openssl_pkey_new;
use function set_error_handler;

/**
 * @coversDefaultClass \CoStack\Reversible\Operation\Security\SignatureAssertion
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class SignatureAssertionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @covers ::collectOpenSslErrors
     * @uses \CoStack\Reversible\Operation\AsymmetricKeys::assertValidEncryptionKey
     * @uses \CoStack\Reversible\json_decode_assoc
     * @uses \CoStack\Reversible\json_encode_strict
     */
    public function testSignatureAssertionReturnsDataIfSignatureIsCorrect(): void
    {
        $value = 'My secret value';

        $privateKey = openssl_pkey_new();
        $publicKeyPem = openssl_pkey_get_details($privateKey)['key'];
        openssl_pkey_export($privateKey, $privateKeyPem);

        $signatureAssertion = new SignatureAssertion($privateKeyPem, $publicKeyPem);
        $intermediate = $signatureAssertion->execute($value);
        $actual = $signatureAssertion->reverse($intermediate);

        $this->assertSame($value, $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::collectOpenSslErrors
     * @uses \CoStack\Reversible\Operation\AsymmetricKeys::assertValidEncryptionKey
     * @uses \CoStack\Reversible\Exception\InvalidPrivateKeyException::__construct
     */
    public function testSignatureAssertionThrowsExceptionIfPrivateKeyIsInvalid(): void
    {
        $this->expectException(InvalidPrivateKeyException::class);

        $signatureAssertion = new SignatureAssertion('not a private key');
        $signatureAssertion->execute('string');
    }

    /**
     * @covers ::__construct
     * @uses \CoStack\Reversible\Operation\AsymmetricKeys::assertValidEncryptionKey
     * @uses \CoStack\Reversible\Exception\InvalidEncryptionKeyException::__construct
     */
    public function testSignatureAssertionThrowsExceptionIfPrivateKeyTypeIsInvalid(): void
    {
        $this->expectException(InvalidEncryptionKeyException::class);

        // @phpstan-ignore-next-line
        new SignatureAssertion(new stdClass());
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @covers ::collectOpenSslErrors
     * @uses \CoStack\Reversible\Operation\AsymmetricKeys::assertValidEncryptionKey
     * @uses \CoStack\Reversible\Exception\InvalidPublicKeyException::__construct
     * @uses \CoStack\Reversible\json_decode_assoc
     * @uses \CoStack\Reversible\json_encode_strict
     */
    public function testSignatureAssertionThrowsExceptionIfPublicKeyIsInvalid(): void
    {
        $this->expectException(InvalidPublicKeyException::class);

        $privateKey = openssl_pkey_new();
        openssl_pkey_export($privateKey, $privateKeyPem);

        $signatureAssertion = new SignatureAssertion($privateKeyPem, 'not a public key');
        $intermediate = $signatureAssertion->execute('string');
        $signatureAssertion->reverse($intermediate);
    }

    /**
     * @covers ::__construct
     * @uses \CoStack\Reversible\Operation\AsymmetricKeys::assertValidEncryptionKey
     * @uses \CoStack\Reversible\Exception\InvalidEncryptionKeyException::__construct
     */
    public function testSignatureAssertionThrowsExceptionIfPublicKeyTypeIsInvalid(): void
    {
        $this->expectException(InvalidEncryptionKeyException::class);

        // @phpstan-ignore-next-line
        new SignatureAssertion(null, new stdClass());
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @covers ::collectOpenSslErrors
     * @uses \CoStack\Reversible\Operation\AsymmetricKeys::assertValidEncryptionKey
     * @uses \CoStack\Reversible\Exception\SignatureAssertionFailedException::__construct
     * @uses \CoStack\Reversible\json_decode_assoc
     * @uses \CoStack\Reversible\json_encode_strict
     */
    public function testSignatureAssertionThrowsExceptionIfSignatureIsInvalid(): void
    {
        $value = 'My secret value';

        $privateKey = openssl_pkey_new();
        $publicKeyPem = openssl_pkey_get_details($privateKey)['key'];
        openssl_pkey_export($privateKey, $privateKeyPem);

        $signatureAssertion = new SignatureAssertion($privateKeyPem, $publicKeyPem);
        $intermediate = $signatureAssertion->execute($value);

        [$data, $signature] = json_decode(base64_decode($intermediate), true);
        $signature = base64_decode($signature);
        $signature .= 'foo';
        $intermediate = base64_encode(json_encode([$data, base64_encode($signature)]));

        $this->expectException(SignatureAssertionFailedException::class);

        $signatureAssertion->reverse($intermediate);
    }

    /**
     * @covers ::__construct
     * @covers ::assertValidEncryptionKey
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @covers ::collectOpenSslErrors
     * @uses \CoStack\Reversible\json_decode_assoc
     * @uses \CoStack\Reversible\json_encode_strict
     */
    public function testSignatureAssertionOperationsFlushOpenSslErrors(): void
    {
        $privateKey = openssl_pkey_new();
        openssl_pkey_export($privateKey, $privateKeyPem);
        $publicKeyPem = openssl_pkey_get_details($privateKey)['key'];

        $signatureAssertion = new SignatureAssertion($privateKeyPem, $publicKeyPem);

        $intermediate = $signatureAssertion->execute('string');

        $this->assertFalse(openssl_error_string());

        $signatureAssertion->reverse($intermediate);

        $this->assertFalse(openssl_error_string());
    }

    public function invalidInputDataProvider(): array
    {
        return [
            'not b64 encoded' => ["\n%invalid b64 string!"],
            'b64 but string' => [base64_encode('this is not valid json')],
            'b64 & json but string' => [base64_encode(json_encode('this is not a json array'))],
            'b64 & json array but count 0' => [base64_encode(json_encode([]))],
            'b64 & json array but count 1' => [base64_encode(json_encode([1]))],
            'b64 & json array but count 3' => [base64_encode(json_encode([1, 2, 3]))],
            'b64 & json array signature not b64' => [base64_encode(json_encode(['string', "\n%invalid b64 string!"]))],
        ];
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @covers ::assertValidEncryptionKey
     * @uses \CoStack\Reversible\json_decode_assoc
     * @uses \CoStack\Reversible\Exception\SignatureAssertionFailedException::__construct
     * @uses \CoStack\Reversible\Exception\JsonDecodeException::__construct
     * @dataProvider invalidInputDataProvider
     */
    public function testSignatureAssertionThrowsAssertionFailedExceptionIfInputIsInvalid(string $input): void
    {
        $privateKey = openssl_pkey_new();
        openssl_pkey_export($privateKey, $privateKeyPem);
        $publicKeyPem = openssl_pkey_get_details($privateKey)['key'];
        $signatureAssertion = new SignatureAssertion($privateKeyPem, $publicKeyPem);

        $this->expectException(SignatureAssertionFailedException::class);

        $signatureAssertion->reverse($input);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::collectOpenSslErrors
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @covers ::assertValidEncryptionKey
     * @uses \CoStack\Reversible\json_decode_assoc
     * @uses \CoStack\Reversible\json_encode_strict
     */
    public function testSignatureAssertionRemovesTemporaryErrorHandlerOnSuccess(): void
    {
        $this->assertCurrentErrorHandlerIsPhpUnitOfDefault();

        $privateKey = openssl_pkey_new();
        openssl_pkey_export($privateKey, $privateKeyPem);
        $publicKeyPem = openssl_pkey_get_details($privateKey)['key'];
        $signatureAssertion = new SignatureAssertion($privateKeyPem, $publicKeyPem);

        $intermediate = $signatureAssertion->execute('some string');

        $this->assertCurrentErrorHandlerIsPhpUnitOfDefault();

        $signatureAssertion->reverse($intermediate);

        $this->assertCurrentErrorHandlerIsPhpUnitOfDefault();
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::collectOpenSslErrors
     * @covers ::assertValidEncryptionKey
     * @uses \CoStack\Reversible\json_decode_assoc
     * @uses \CoStack\Reversible\json_encode_strict
     * @uses \CoStack\Reversible\Exception\InvalidPrivateKeyException::__construct
     */
    public function testSignatureAssertionExecutionRemovesTemporaryErrorHandlerOnFailure(): void
    {
        $this->assertCurrentErrorHandlerIsPhpUnitOfDefault();

        $signatureAssertion = new SignatureAssertion('not a private key');
        try {
            $signatureAssertion->execute('string');
        } /** @noinspection PhpRedundantCatchClauseInspection */ catch (InvalidPrivateKeyException $exception) {
            // Discard exception
        }

        $this->assertCurrentErrorHandlerIsPhpUnitOfDefault();
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @covers ::collectOpenSslErrors
     * @covers ::assertValidEncryptionKey
     * @uses \CoStack\Reversible\json_decode_assoc
     * @uses \CoStack\Reversible\json_encode_strict
     * @uses \CoStack\Reversible\Exception\InvalidPublicKeyException::__construct
     */
    public function testSignatureAssertionReversionRemovesTemporaryErrorHandlerOnFailure(): void
    {
        $this->assertCurrentErrorHandlerIsPhpUnitOfDefault();

        $privateKey = openssl_pkey_new();
        openssl_pkey_export($privateKey, $privateKeyPem);

        $signatureAssertion = new SignatureAssertion($privateKeyPem, 'not a public key');
        $intermediate = $signatureAssertion->execute('string');
        try {
            $signatureAssertion->reverse($intermediate);
        } /** @noinspection PhpRedundantCatchClauseInspection */ catch (InvalidPublicKeyException $exception) {
            // Discard exception
        }

        $this->assertCurrentErrorHandlerIsPhpUnitOfDefault();
    }

    /**
     * @covers ::__construct
     * @covers ::assertValidEncryptionKey
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\Exception\SignatureAssertionFailedException::__construct
     * @uses \CoStack\Reversible\json_decode_assoc
     */
    public function testSignatureAssertionReversionThrowsExceptionIfDataIsNotString(): void
    {
        $value = base64_encode(json_encode([1, 'string']));

        $signatureAssertion = new SignatureAssertion();

        $this->expectException(SignatureAssertionFailedException::class);

        $signatureAssertion->reverse($value);
    }

    /**
     * @covers ::__construct
     * @covers ::assertValidEncryptionKey
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\Exception\SignatureAssertionFailedException::__construct
     * @uses \CoStack\Reversible\json_decode_assoc
     */
    public function testSignatureAssertionReversionThrowsExceptionIfSignatureIsNotString(): void
    {
        $value = base64_encode(json_encode(['string', 1]));

        $signatureAssertion = new SignatureAssertion();

        $this->expectException(SignatureAssertionFailedException::class);

        $signatureAssertion->reverse($value);
    }

    protected function assertCurrentErrorHandlerIsPhpUnitOfDefault(): void
    {
        $firstErrorHandler = set_error_handler(null);
        $this->assertInstanceOf(ErrorHandler::class, $firstErrorHandler);
        $secondErrorHandler = set_error_handler(null);
        $this->assertNull($secondErrorHandler, 'The current error handler is unexpected');
        // restore original error handler
        set_error_handler($firstErrorHandler);
    }
}
