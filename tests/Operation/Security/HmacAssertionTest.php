<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Operation\Security;

use CoStack\Reversible\Exception\HmacAssertionFailedException;
use CoStack\Reversible\Operation\Security\HmacAssertion;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Operation\Security\HmacAssertion
 */
class HmacAssertionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testHmacAssertionSucceedsIfHmacIsCorrect(): void
    {
        $hmacAssertion = new HmacAssertion('secret password');
        $intermediate = $hmacAssertion->execute('my secret foo bar');
        $actual = $hmacAssertion->reverse($intermediate);

        $this->assertSame('my secret foo bar', $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\Exception\HmacAssertionFailedException::__construct
     */
    public function testHmacAssertionThrowsExceptionIfSecretIsWrong(): void
    {
        $hmacAssertion = new HmacAssertion('secret password');
        $intermediate = $hmacAssertion->execute('my secret foo bar');

        $this->expectException(HmacAssertionFailedException::class);

        $hmacAssertion = new HmacAssertion('secret changed');
        $hmacAssertion->reverse($intermediate);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\Exception\HmacAssertionFailedException::__construct
     */
    public function testHmacAssertionThrowsExceptionIfMessageIsChanged(): void
    {
        $hmacAssertion = new HmacAssertion('secret password');
        $intermediate = $hmacAssertion->execute('my secret foo bar');

        $intermediate .= 'hacker alarm';

        $this->expectException(HmacAssertionFailedException::class);

        $hmacAssertion = new HmacAssertion('secret password');
        $hmacAssertion->reverse($intermediate);
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\Exception\HmacAssertionFailedException::__construct
     */
    public function testHmacAssertionThrowsErrorIfValueDoesNotContainSplitCharacter(): void
    {
        $hmacAssertion = new HmacAssertion('secret password', 'sha1', '`');

        $this->expectException(HmacAssertionFailedException::class);

        $hmacAssertion->reverse('invalid');
    }
}
