<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Operation\Transform;

use CoStack\Reversible\Exception\ArrayIsNotSequentialException;
use CoStack\Reversible\Exception\EmptyStringException;
use CoStack\Reversible\Operation\Transform\ImplodeTransform;
use CoStack\Reversible\TypeLossy;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Operation\Transform\ImplodeTransform
 */
class ImplodeTransformTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     */
    public function testImplodeTransformImplodesValuesWithGivenDelimiter(): void
    {
        $value = [1, 2, 'foo', 4, 'bar'];

        $implodeTransform = new ImplodeTransform('`');
        $actual = $implodeTransform->execute($value);

        $this->assertSame('1`2`foo`4`bar', $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testImplodeTransformExplodesStringByGivenDelimiter(): void
    {
        $value = '1`2`foo`4`bar';

        $implodeTransform = new ImplodeTransform('`');
        $actual = $implodeTransform->reverse($value);

        $this->assertSame(['1', '2', 'foo', '4', 'bar'], $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @uses \CoStack\Reversible\Exception\ArrayIsNotSequentialException::__construct
     */
    public function testImplodeTransformThrowsExceptionIfArrayIsAssociative(): void
    {
        $this->expectException(ArrayIsNotSequentialException::class);

        $value = [
            'foo' => 'bar',
        ];

        $implodeTransform = new ImplodeTransform();
        $implodeTransform->execute($value);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @uses \CoStack\Reversible\Exception\EmptyStringException::__construct
     */
    public function testImplodeTransformThrowsExceptionIfDelimiterIsEmpty(): void
    {
        $this->expectException(EmptyStringException::class);
        $this->expectExceptionMessage('Argument 0 passed to CoStack\Reversible\Operation\Transform\ImplodeTransform::__construct must be a non-empty string, an empty string was given');

        // @phpstan-ignore-next-line
        new ImplodeTransform('');
    }

    /**
     * @covers ::__construct
     */
    public function testImplodeTransformIsLossy(): void
    {
        $implodeTransform = new ImplodeTransform();

        $this->assertInstanceOf(TypeLossy::class, $implodeTransform);
    }
}
