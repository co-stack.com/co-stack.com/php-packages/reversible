<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Operation\Data;

use CoStack\Reversible\Exception\InvalidArgumentException;
use CoStack\Reversible\Exception\JsonDecodeException;
use CoStack\Reversible\Operation\Data\SpliceData;
use PHPUnit\Framework\TestCase;

use function bin2hex;
use function random_bytes;

/**
 * @coversDefaultClass \CoStack\Reversible\Operation\Data\SpliceData
 */
class SpliceDataTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\json_decode_assoc
     */
    public function testSpliceInsertsAndRemovesDataFromResult(): void
    {
        $canary = bin2hex(random_bytes(16));

        $execution = new SpliceData($canary);

        $intermediate = $execution->execute('hello world');

        $myVariable = '';

        $reversion = new SpliceData($myVariable);

        $leftOver = $reversion->reverse($intermediate);

        $this->assertSame('hello world', $leftOver);
        $this->assertSame($canary, $myVariable);
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\json_decode_assoc
     * @uses \CoStack\Reversible\Exception\InvalidArgumentException
     */
    public function testSpliceDataThrowsExceptionIfTheInputIsMalformed(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $canary = '';
        $reversible = new SpliceData($canary);

        $reversible->reverse('["only one element"]');
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\json_decode_assoc
     * @uses \CoStack\Reversible\Exception\JsonDecodeException
     */
    public function testSpliceDataThrowsExceptionIfTheInputIsInvalidJson(): void
    {
        $this->expectException(JsonDecodeException::class);

        $canary = '';
        $reversible = new SpliceData($canary);

        $reversible->reverse('invalid json');
    }
}
