<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Operation\Encoding;

use CoStack\Reversible\Operation\Encoding\HexToBinEncoding;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Operation\Encoding\HexToBinEncoding
 */
class HexToBinEncodingTest extends TestCase
{
    /**
     * @covers ::execute
     * @covers ::getExecutionClosure
     */
    public function testHexToBinEncodingEncodesHexToBin(): void
    {
        $expected = random_bytes(16);
        $value = bin2hex($expected);
        $hexToBinEncoding = new HexToBinEncoding();
        $actual = $hexToBinEncoding->execute($value);

        $this->assertSame($expected, $actual);
    }

    /**
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testHexToBinEncodingDecodesBinToHex(): void
    {
        $value = ')PY�O�js\'1rZ���';
        $hexToBinEncoding = new HexToBinEncoding();
        $actual = $hexToBinEncoding->reverse($value);

        $this->assertSame('295059efbfbd4fefbfbd6a732731725a08efbfbdefbfbdefbfbd', $actual);
    }
}
