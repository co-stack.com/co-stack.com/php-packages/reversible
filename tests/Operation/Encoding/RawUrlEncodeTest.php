<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Operation\Encoding;

use CoStack\Reversible\Operation\Encoding\RawUrlEncode;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Operation\Encoding\RawUrlEncode
 */
class RawUrlEncodeTest extends TestCase
{
    /**
     * @covers ::execute
     * @covers ::getExecutionClosure
     */
    public function testRawUrlEncodeEncodesString(): void
    {
        $value = 'hello$world§foo&bar%baz?thing';

        $rawUrlEncode = new RawUrlEncode();
        $actual = $rawUrlEncode->execute($value);

        $this->assertSame('hello%24world%C2%A7foo%26bar%25baz%3Fthing', $actual);
    }

    /**
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testRawUrlEncodeDecodesString(): void
    {
        $value = 'hello%24world%C2%A7foo%26bar%25baz%3Fthing';

        $rawUrlEncode = new RawUrlEncode();
        $actual = $rawUrlEncode->reverse($value);

        $this->assertSame('hello$world§foo&bar%baz?thing', $actual);
    }
}
