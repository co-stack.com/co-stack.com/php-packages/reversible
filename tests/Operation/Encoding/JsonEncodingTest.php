<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Operation\Encoding;

use CoStack\Reversible\Exception\InvalidArgumentTypeException;
use CoStack\Reversible\Exception\JsonDecodeException;
use CoStack\Reversible\Exception\JsonEncodeException;
use CoStack\Reversible\Operation\Encoding\JsonEncoding;
use CoStack\Reversible\TypeLossy;
use PHPUnit\Framework\TestCase;

use function fopen;

/**
 * @coversDefaultClass \CoStack\Reversible\Operation\Encoding\JsonEncoding
 */
class JsonEncodingTest extends TestCase
{
    /**
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @uses \CoStack\Reversible\json_encode_strict
     */
    public function testJsonEncodingEncodesArrayToJson(): void
    {
        $value = [
            1,
            2,
            'foof',
            'baab',
        ];

        $jsonEncoding = new JsonEncoding();
        $actual = $jsonEncoding->execute($value);

        $this->assertSame('[1,2,"foof","baab"]', $actual);
    }

    /**
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\json_decode_assoc
     */
    public function testJsonEncodingDecodesStringToArray(): void
    {
        $value = '[1,2,"foof","baab"]';

        $jsonEncoding = new JsonEncoding();
        $actual = $jsonEncoding->reverse($value);

        $this->assertSame([1, 2, 'foof', 'baab',], $actual);
    }

    /**
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @uses \CoStack\Reversible\Exception\InvalidArgumentTypeException::__construct
     * @uses \CoStack\Reversible\json_encode_strict
     */
    public function testJsonEncodingThrowsExceptionIfEncodingValueIsNotScalarOrArray(): void
    {
        $this->expectException(InvalidArgumentTypeException::class);

        $value = fopen('php://temp', 'r');

        $jsonEncoding = new JsonEncoding();
        $jsonEncoding->execute($value);
    }

    /**
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @uses \CoStack\Reversible\Exception\JsonEncodeException::__construct
     * @uses \CoStack\Reversible\json_encode_strict
     */
    public function testJsonEncodingThrowsExceptionIfEncodingValueCouldNotBeEncoded(): void
    {
        $this->expectException(JsonEncodeException::class);

        $value = "\xB1\x31";

        $jsonEncoding = new JsonEncoding();
        $jsonEncoding->execute($value);
    }

    /**
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\Exception\JsonDecodeException::__construct
     * @uses \CoStack\Reversible\json_decode_assoc
     */
    public function testJsonEncodingThrowsExceptionIfValueCouldNOtBeDecoded(): void
    {
        $this->expectException(JsonDecodeException::class);

        $value = "[[}invalid";

        $jsonEncoding = new JsonEncoding();
        $jsonEncoding->reverse($value);
    }

    /**
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\json_decode_assoc
     * @uses \CoStack\Reversible\json_encode_strict
     */
    public function testJsonEncodeIsNotTypeLossy(): void
    {
        $jsonEncoding = new JsonEncoding();

        $this->assertNotInstanceOf(TypeLossy::class, $jsonEncoding);

        $intermediate = $jsonEncoding->execute(1);
        $actual = $jsonEncoding->reverse($intermediate);

        $this->assertSame(1, $actual);
    }
}
