<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Operation\Encoding;

use __PHP_Incomplete_Class;
use CoStack\Reversible\Exception\InvalidArgumentTypeException;
use CoStack\Reversible\Exception\UnserializeFailedException;
use CoStack\Reversible\Operation\Encoding\SerializationEncoding;
use PHPUnit\Framework\TestCase;
use PHPUnit\Util\ErrorHandler;
use stdClass;

use function fopen;
use function set_error_handler;

/**
 * @coversDefaultClass \CoStack\Reversible\Operation\Encoding\SerializationEncoding
 */
class SerializationEncodingTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     */
    public function testSerializationEncodingSerializesObject(): void
    {
        $value = new stdClass();
        $value->property1 = 'foo';

        $operation = new SerializationEncoding();
        $actual = $operation->execute($value);

        $this->assertSame('O:8:"stdClass":1:{s:9:"property1";s:3:"foo";}', $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSerializationEncodingUnserializesObject(): void
    {
        $value = 'O:8:"stdClass":1:{s:9:"property1";s:3:"foo";}';

        $operation = new SerializationEncoding([stdClass::class]);
        $actual = $operation->reverse($value);

        $expected = new stdClass();
        $expected->property1 = 'foo';

        // Not same, because serialization duplicates objects
        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @uses \CoStack\Reversible\Exception\InvalidArgumentTypeException::__construct
     */
    public function testSerializationEncodingThrowsExceptionIfValueIsNotSerializable(): void
    {
        $this->expectException(InvalidArgumentTypeException::class);

        $operation = new SerializationEncoding();
        $operation->execute(fopen('php://input', 'r'));
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSerializationEncodingReturnsIncompleteClassWhenDeserializationIsNotAllowed(): void
    {
        $value = new stdClass();

        $operation = new SerializationEncoding([]);
        $intermediate = $operation->execute($value);
        $actual = $operation->reverse($intermediate);

        $this->assertInstanceOf(__PHP_Incomplete_Class::class, $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSerializationEncodingDefaultsToSecureSetting(): void
    {
        $value = new stdClass();

        $operation = new SerializationEncoding();
        $intermediate = $operation->execute($value);
        $actual = $operation->reverse($intermediate);

        $this->assertInstanceOf(__PHP_Incomplete_Class::class, $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSerializationEncodingResetsInternalErrorHandler(): void
    {
        $operation = new SerializationEncoding();
        $intermediate = $operation->execute('foo');

        $this->assertCurrentErrorHandlerIsPhpUnitOfDefault();

        $operation->reverse($intermediate);

        $this->assertCurrentErrorHandlerIsPhpUnitOfDefault();
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\Exception\UnserializeFailedException
     */
    public function testSerializationEncodingThrowsExceptionIfSerializedStringIsMalformed(): void
    {
        $operation = new SerializationEncoding();

        $this->expectException(UnserializeFailedException::class);

        $operation->reverse('not a valid serialized string');
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\Exception\UnserializeFailedException
     */
    public function testSerializationEncodingRestoresErrorHandlerIfExceptionWasThrown(): void
    {
        $operation = new SerializationEncoding();

        try {
            $operation->reverse('not a valid serialized string');
        } /** @noinspection PhpRedundantCatchClauseInspection */ catch (UnserializeFailedException $exception) {
            // Ignore expected exception
        }

        $this->assertCurrentErrorHandlerIsPhpUnitOfDefault();
    }

    protected function assertCurrentErrorHandlerIsPhpUnitOfDefault(): void
    {
        $firstErrorHandler = set_error_handler(null);
        $this->assertInstanceOf(ErrorHandler::class, $firstErrorHandler);
        $secondErrorHandler = set_error_handler(null);
        $this->assertNull($secondErrorHandler, 'The current error handler is unexpected');
        // restore original error handler
        set_error_handler($firstErrorHandler);
    }
}
