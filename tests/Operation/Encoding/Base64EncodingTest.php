<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Operation\Encoding;

use CoStack\Reversible\Exception\InvalidArgumentTypeException;
use CoStack\Reversible\Operation\Encoding\Base64Encoding;
use CoStack\Reversible\TypeLossy;
use PHPUnit\Framework\TestCase;
use stdClass;

/**
 * @coversDefaultClass \CoStack\Reversible\Operation\Encoding\Base64Encoding
 */
class Base64EncodingTest extends TestCase
{
    /**
     * @covers ::execute
     * @covers ::getExecutionClosure
     */
    public function testBase64EncodingEncodesStringInBase64(): void
    {
        $string = 'Hello World, quick fox jump!';
        $base64Encoding = new Base64Encoding();
        $actual = $base64Encoding->execute($string);

        $this->assertSame('SGVsbG8gV29ybGQsIHF1aWNrIGZveCBqdW1wIQ==', $actual);
    }

    /**
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testBase64EncodingDecodesIntoString(): void
    {
        $string = 'SGVsbG8gV29ybGQsIHF1aWNrIGZveCBqdW1wIQ==';
        $base64Encoding = new Base64Encoding();
        $actual = $base64Encoding->reverse($string);

        $this->assertSame('Hello World, quick fox jump!', $actual);
    }

    /**
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @uses \CoStack\Reversible\Exception\InvalidArgumentTypeException::__construct
     */
    public function testBase64EncodingThrowsExceptionIfValueIsNotScalar(): void
    {
        $this->expectException(InvalidArgumentTypeException::class);

        $base64Encoding = new Base64Encoding();
        $base64Encoding->execute(new stdClass());
    }

    /**
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testBase64EncodingIsLossy(): void
    {
        $base64Encoding = new Base64Encoding();

        $this->assertInstanceOf(TypeLossy::class, $base64Encoding);

        $intermediate = $base64Encoding->execute(1);
        $actual = $base64Encoding->reverse($intermediate);

        $this->assertSame('1', $actual);
    }
}
