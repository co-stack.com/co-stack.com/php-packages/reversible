<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests;

use Closure;
use CoStack\Reversible\AbstractReversible;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\AbstractReversible
 */
class AbstractReversibleTest extends TestCase
{
    /**
     * @covers ::execute
     */
    public function testAbstractReversibleExecuteCallsGetExecutionClosureWithValue(): void
    {
        $calls = [];

        $abstractReversible = new class ($calls) extends AbstractReversible {
            protected $calls;

            public function __construct(&$calls)
            {
                $this->calls = &$calls;
            }

            public function getExecutionClosure(): Closure
            {
                return function ($value) {
                    $this->calls[] = ['execute', $value];
                };
            }

            public function getReversionClosure(): Closure
            {
                return function ($value) {
                    $this->calls[] = ['reverse', $value];
                };
            }
        };

        $abstractReversible->execute('foo');

        $this->assertSame([['execute', 'foo']], $calls);
    }

    /**
     * @covers ::reverse
     */
    public function testAbstractReversibleExecuteCallsGetReversionClosureWithValue(): void
    {
        $calls = [];

        $abstractReversible = new class ($calls) extends AbstractReversible {
            protected $calls;

            public function __construct(&$calls)
            {
                $this->calls = &$calls;
            }

            public function getExecutionClosure(): Closure
            {
                return function ($value) {
                    $this->calls[] = ['execute', $value];
                };
            }

            public function getReversionClosure(): Closure
            {
                return function ($value) {
                    $this->calls[] = ['reverse', $value];
                };
            }
        };

        $abstractReversible->reverse('foo');

        $this->assertSame([['reverse', 'foo']], $calls);
    }
}
