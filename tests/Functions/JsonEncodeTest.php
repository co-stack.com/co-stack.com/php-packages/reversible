<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Functions;

use CoStack\Reversible\Exception\InvalidArgumentTypeException;
use CoStack\Reversible\Exception\JsonEncodeException;
use PHPUnit\Framework\TestCase;

use function CoStack\Reversible\json_encode_strict;
use function fopen;
use function json_decode;

class JsonEncodeTest extends TestCase
{
    /**
     * @covers \CoStack\Reversible\json_encode_strict
     */
    public function testJsonEncodeReturnsString(): void
    {
        $expected = [1, 2, 'string'];
        $encoded = json_encode_strict($expected);
        $actual = json_decode($encoded, true);
        $this->assertSame($expected, $actual);
    }

    /**
     * @covers \CoStack\Reversible\json_encode_strict
     * @uses   \CoStack\Reversible\Exception\JsonEncodeException::__construct
     */
    public function testJsonEncodeThrowsExceptionIfValueCanNotBeEncoded(): void
    {
        $this->expectException(JsonEncodeException::class);

        json_encode_strict(pack('H*', md5('foo')));
    }

    /**
     * @covers \CoStack\Reversible\json_encode_strict
     * @uses   \CoStack\Reversible\Exception\InvalidArgumentTypeException::__construct
     */
    public function testJsonEncodeThrowsExceptionIfInputCanNotBeEncoded(): void
    {
        $this->expectException(InvalidArgumentTypeException::class);

        json_encode_strict(fopen('php://temp', 'r'));
    }
}
