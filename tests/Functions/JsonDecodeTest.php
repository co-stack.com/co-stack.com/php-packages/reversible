<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Functions;

use CoStack\Reversible\Exception\JsonDecodeException;
use PHPUnit\Framework\TestCase;

use function CoStack\Reversible\json_decode_assoc;
use function json_encode;

class JsonDecodeTest extends TestCase
{
    /**
     * @covers \CoStack\Reversible\json_decode_assoc
     */
    public function testJsonDecodeReturnsData(): void
    {
        $expected = [1, 2, 'string'];
        $encoded = json_encode($expected);
        $actual = json_decode_assoc($encoded);
        $this->assertSame($expected, $actual);
    }

    /**
     * @covers \CoStack\Reversible\json_decode_assoc
     * @uses   \CoStack\Reversible\Exception\JsonDecodeException::__construct
     */
    public function testJsonDecodeThrowsExceptionOnError(): void
    {
        $this->expectException(JsonDecodeException::class);

        json_decode_assoc('invalid json');
    }
}
