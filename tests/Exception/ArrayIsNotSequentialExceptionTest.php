<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\ArrayIsNotSequentialException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\ArrayIsNotSequentialException
 */
class ArrayIsNotSequentialExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::getMethod
     */
    public function testArrayIsNotSequentialExceptionConstruction(): void
    {
        $exception = new ArrayIsNotSequentialException('MyMethod');

        $this->assertSame(
            'The array passed to "MyMethod" is not sequential (it has associative keys).',
            $exception->getMessage()
        );
        $this->assertSame('MyMethod', $exception->getMethod());
    }

    /**
     * @coversNothing
     */
    public function testArrayIsNotSequentialExceptionCode(): void
    {
        $this->assertSame(1581089760, ArrayIsNotSequentialException::CODE);
    }
}
