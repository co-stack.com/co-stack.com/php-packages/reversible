<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\DecryptionFailedException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\DecryptionFailedException
 */
class DecryptionFailedExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::getErrors
     */
    public function testDecryptionFailedException(): void
    {
        $exception = new DecryptionFailedException(['error 1', 'error 2']);
        $this->assertSame(
            "The encrypted text could not be decrypted. Errors:\nerror 1\nerror 2",
            $exception->getMessage()
        );
        $this->assertSame(['error 1', 'error 2'], $exception->getErrors());
    }
}
