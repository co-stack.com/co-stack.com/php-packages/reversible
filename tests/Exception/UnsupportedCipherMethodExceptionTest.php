<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\UnsupportedCipherMethodException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\UnsupportedCipherMethodException
 */
class UnsupportedCipherMethodExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::getAlgo
     * @covers ::getAvailable
     */
    public function testDecryptionFailedException(): void
    {
        $exception = new UnsupportedCipherMethodException('Unsupported algo', ['available 1', 'available 2']);

        $this->assertSame(
            'The cypher algorithm "Unsupported algo" is not in te list of supported algorithms.',
            $exception->getMessage()
        );
        $this->assertSame('Unsupported algo', $exception->getAlgo());
        $this->assertSame(['available 1', 'available 2'], $exception->getAvailable());
    }
}
