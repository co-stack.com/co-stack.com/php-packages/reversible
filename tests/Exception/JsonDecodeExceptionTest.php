<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\JsonDecodeException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\JsonDecodeException
 */
class JsonDecodeExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::getErrorCode
     * @covers ::getErrorMessage
     * @covers ::getValue
     */
    public function testArrayIsNotSequentialExceptionConstruction(): void
    {
        $exception = new JsonDecodeException(12345, 'MyError', 'A value');

        $this->assertSame('json_decode error [12345]: "MyError"', $exception->getMessage());
        $this->assertSame(12345, $exception->getErrorCode());
        $this->assertSame('MyError', $exception->getErrorMessage());
        $this->assertSame('A value', $exception->getValue());
    }

    /**
     * @coversNothing
     */
    public function testArrayIsNotSequentialExceptionCode(): void
    {
        $this->assertSame(1621340544, JsonDecodeException::CODE);
    }
}
