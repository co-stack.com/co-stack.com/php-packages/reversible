<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use PHPUnit\Framework\TestCase;

/**
 * @coversNothing
 */
class UniqueExceptionCodeTest extends TestCase
{
    public function testAllExceptionCodesAreUnique(): void
    {
        $codes = [];
        $results = scandir(__DIR__ . '/../../src/Exception');
        $exceptions = array_diff($results, ['..', '.']);
        foreach ($exceptions as $exception) {
            $fqcn = 'CoStack\\Reversible\\Exception\\' . substr($exception, 0, -4);
            $exceptionCode = constant($fqcn . '::CODE');
            $this->assertNotContains($exceptionCode, $codes);
            $codes[$exceptionCode] = $exceptionCode;
        }
    }
}
