<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\HmacAssertionFailedException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\HmacAssertionFailedException
 */
class HmacAssertionFailedExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     */
    public function testArrayIsNotSequentialExceptionConstruction(): void
    {
        $exception = new HmacAssertionFailedException();

        $this->assertSame('HMAC validation failed', $exception->getMessage());
    }

    /**
     * @coversNothing
     */
    public function testArrayIsNotSequentialExceptionCode(): void
    {
        $this->assertSame(1605190985, HmacAssertionFailedException::CODE);
    }
}
