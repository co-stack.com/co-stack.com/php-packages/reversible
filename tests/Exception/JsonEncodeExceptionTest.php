<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\JsonEncodeException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\JsonEncodeException
 */
class JsonEncodeExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::getErrorCode
     * @covers ::getErrorMessage
     * @covers ::getValue
     */
    public function testArrayIsNotSequentialExceptionConstruction(): void
    {
        $exception = new JsonEncodeException(12345, 'MyError', 'A value');

        $this->assertSame('json_encode error [12345]: "MyError"', $exception->getMessage());
        $this->assertSame(12345, $exception->getErrorCode());
        $this->assertSame('MyError', $exception->getErrorMessage());
        $this->assertSame('A value', $exception->getValue());
    }

    /**
     * @coversNothing
     */
    public function testArrayIsNotSequentialExceptionCode(): void
    {
        $this->assertSame(1621340411, JsonEncodeException::CODE);
    }
}
