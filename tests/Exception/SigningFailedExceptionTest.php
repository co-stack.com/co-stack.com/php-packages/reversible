<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\SigningFailedException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\SigningFailedException
 */
class SigningFailedExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::getErrors
     */
    public function testArrayIsNotSequentialExceptionConstruction(): void
    {
        $exception = new SigningFailedException(['err foo', 'bar err']);

        $this->assertSame('Creating the signature failed', $exception->getMessage());
        $this->assertSame(['err foo', 'bar err'], $exception->getErrors());
    }

    /**
     * @coversNothing
     */
    public function testArrayIsNotSequentialExceptionCode(): void
    {
        $this->assertSame(1622131620, SigningFailedException::CODE);
    }
}
