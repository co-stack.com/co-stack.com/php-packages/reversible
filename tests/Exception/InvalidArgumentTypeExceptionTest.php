<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\InvalidArgumentTypeException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\InvalidArgumentTypeException
 */
class InvalidArgumentTypeExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::getName
     * @covers ::getExpected
     * @covers ::getActual
     */
    public function testArrayIsNotSequentialExceptionConstruction(): void
    {
        $exception = new InvalidArgumentTypeException('A name', 'A string', 'Another string');

        $this->assertSame(
            'Argument "A name" is expected to be of type "A string", "Another string" given',
            $exception->getMessage()
        );
        $this->assertSame('A name', $exception->getName());
        $this->assertSame('A string', $exception->getExpected());
        $this->assertSame('Another string', $exception->getActual());
    }

    /**
     * @coversNothing
     */
    public function testArrayIsNotSequentialExceptionCode(): void
    {
        $this->assertSame(1581073983, InvalidArgumentTypeException::CODE);
    }
}
