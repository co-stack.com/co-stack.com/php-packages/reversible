<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\EmptyStringException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\EmptyStringException
 */
class EmptyStringExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::getPosition
     * @covers ::getFunction
     */
    public function testDecryptionFailedException(): void
    {
        $exception = new EmptyStringException(2, 'fooBar');
        $this->assertSame(
            "Argument 2 passed to fooBar must be a non-empty string, an empty string was given",
            $exception->getMessage()
        );
        $this->assertSame(2, $exception->getPosition());
        $this->assertSame('fooBar', $exception->getFunction());
    }
}
