<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\UnserializeFailedException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\UnserializeFailedException
 */
class UnserializeFailedExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::getErrorCode
     * @covers ::getErrorMessage
     * @covers ::getInput
     */
    public function testUnserializeFailedExceptionConstruction(): void
    {
        $exception = new UnserializeFailedException(1631608557, 'foo bar baz', 'a string');

        $this->assertSame('1631608557: foo bar baz', $exception->getMessage());
        $this->assertSame(1631608557, $exception->getErrorCode());
        $this->assertSame('foo bar baz', $exception->getErrorMessage());
        $this->assertSame('a string', $exception->getInput());
    }

    /**
     * @coversNothing
     */
    public function testUnserializeFailedExceptionCode(): void
    {
        $this->assertSame(1631606863, UnserializeFailedException::CODE);
    }
}
