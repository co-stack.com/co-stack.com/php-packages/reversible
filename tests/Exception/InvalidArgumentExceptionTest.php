<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\InvalidArgumentException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\InvalidArgumentException
 */
class InvalidArgumentExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::getReason
     */
    public function testArrayIsNotSequentialExceptionConstruction(): void
    {
        $exception = new InvalidArgumentException('A reason');

        $this->assertSame(
            'The given argument is invalid: A reason',
            $exception->getMessage()
        );
        $this->assertSame('A reason', $exception->getReason());
    }

    /**
     * @coversNothing
     */
    public function testArrayIsNotSequentialExceptionCode(): void
    {
        $this->assertSame(1627026950, InvalidArgumentException::CODE);
    }
}
