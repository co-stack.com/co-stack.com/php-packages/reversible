<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\MissingPublicKeyException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\MissingPublicKeyException
 */
class MissingPublicKeyExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     */
    public function testArrayIsNotSequentialExceptionConstruction(): void
    {
        $exception = new MissingPublicKeyException();

        $this->assertSame('You must set a public key if you want to encrypt a value', $exception->getMessage());
    }

    /**
     * @coversNothing
     */
    public function testArrayIsNotSequentialExceptionCode(): void
    {
        $this->assertSame(1621360764, MissingPublicKeyException::CODE);
    }
}
