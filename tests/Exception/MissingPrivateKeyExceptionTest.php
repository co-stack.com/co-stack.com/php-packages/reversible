<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\MissingPrivateKeyException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\MissingPrivateKeyException
 */
class MissingPrivateKeyExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     */
    public function testArrayIsNotSequentialExceptionConstruction(): void
    {
        $exception = new MissingPrivateKeyException();

        $this->assertSame('You must set a private key if you want to decrypt a value', $exception->getMessage());
    }

    /**
     * @coversNothing
     */
    public function testArrayIsNotSequentialExceptionCode(): void
    {
        $this->assertSame(1621360864, MissingPrivateKeyException::CODE);
    }
}
