<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\SignatureAssertionFailedException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\SignatureAssertionFailedException
 */
class SignatureAssertionFailedExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     */
    public function testArrayIsNotSequentialExceptionConstruction(): void
    {
        $exception = new SignatureAssertionFailedException();

        $this->assertSame('The signature is invalid', $exception->getMessage());
    }

    /**
     * @coversNothing
     */
    public function testArrayIsNotSequentialExceptionCode(): void
    {
        $this->assertSame(1622134627, SignatureAssertionFailedException::CODE);
    }
}
