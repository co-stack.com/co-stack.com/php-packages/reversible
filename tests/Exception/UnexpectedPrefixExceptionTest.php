<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\UnexpectedPrefixException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\UnexpectedPrefixException
 */
class UnexpectedPrefixExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::getPrefix
     * @covers ::getString
     */
    public function testArrayIsNotSequentialExceptionConstruction(): void
    {
        $exception = new UnexpectedPrefixException('The prefix', 'The string');

        $this->assertSame(
            'The string "The string" was not prefixed with the expected prefix "The prefix"',
            $exception->getMessage()
        );
        $this->assertSame('The prefix', $exception->getPrefix());
        $this->assertSame('The string', $exception->getString());
    }

    /**
     * @coversNothing
     */
    public function testArrayIsNotSequentialExceptionCode(): void
    {
        $this->assertSame(1621342347, UnexpectedPrefixException::CODE);
    }
}
