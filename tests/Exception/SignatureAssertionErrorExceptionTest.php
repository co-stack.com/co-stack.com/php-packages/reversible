<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\SignatureAssertionErrorException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\SignatureAssertionErrorException
 */
class SignatureAssertionErrorExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::getErrors
     */
    public function testArrayIsNotSequentialExceptionConstruction(): void
    {
        $exception = new SignatureAssertionErrorException(['foo err', 'bar err']);

        $this->assertSame('An error occurred during signature validation', $exception->getMessage());
        $this->assertSame(['foo err', 'bar err'], $exception->getErrors());
    }

    /**
     * @coversNothing
     */
    public function testArrayIsNotSequentialExceptionCode(): void
    {
        $this->assertSame(1622130646, SignatureAssertionErrorException::CODE);
    }
}
