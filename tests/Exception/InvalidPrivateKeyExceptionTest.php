<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\InvalidPrivateKeyException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\InvalidPrivateKeyException
 */
class InvalidPrivateKeyExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::getKey
     * @covers ::getErrors
     */
    public function testArrayIsNotSequentialExceptionConstruction(): void
    {
        $exception = new InvalidPrivateKeyException(['err 1', 'err 2'], 'not a valid key');

        $this->assertSame('The given string is not a valid private key', $exception->getMessage());
        $this->assertSame('not a valid key', $exception->getKey());
        $this->assertSame(['err 1', 'err 2'], $exception->getErrors());
    }

    /**
     * @coversNothing
     */
    public function testInvalidPrivateKeyExceptionCode(): void
    {
        $this->assertSame(1622127061, InvalidPrivateKeyException::CODE);
    }
}
