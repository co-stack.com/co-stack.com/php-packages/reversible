<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\RangeException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\RangeException
 */
class RangeExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::getGiven
     * @covers ::getLow
     * @covers ::getHigh
     */
    public function testArrayIsNotSequentialExceptionConstruction(): void
    {
        $exception = new RangeException(15, 1, 7);

        $this->assertSame('The integer 15 is not in the expected range from 1 to 7', $exception->getMessage());
        $this->assertSame(15, $exception->getGiven());
        $this->assertSame(1, $exception->getLow());
        $this->assertSame(7, $exception->getHigh());
    }

    /**
     * @coversNothing
     */
    public function testRangeExceptionExceptionCode(): void
    {
        $this->assertSame(1621598353, RangeException::CODE);
    }
}
