<?php

declare(strict_types=1);

namespace CoStack\ReversibleTests\Exception;

use CoStack\Reversible\Exception\InvalidEncryptionKeyException;
use PHPUnit\Framework\TestCase;
use stdClass;

/**
 * @coversDefaultClass \CoStack\Reversible\Exception\InvalidEncryptionKeyException
 */
class InvalidEncryptionKeyExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::getExpected
     * @covers ::getUsage
     * @covers ::getKey
     */
    public function testArrayIsNotSequentialExceptionConstruction(): void
    {
        $stdClass = new stdClass();
        $exception = new InvalidEncryptionKeyException('typeA|typeB', 'for something', $stdClass);

        $this->assertSame(
            'The for something key is expected to be of type typeA|typeB but is a "stdClass"',
            $exception->getMessage()
        );
        $this->assertSame('typeA|typeB', $exception->getExpected());
        $this->assertSame('for something', $exception->getUsage());
        $this->assertSame($stdClass, $exception->getKey());
    }

    /**
     * @coversNothing
     */
    public function testArrayIsNotSequentialExceptionCode(): void
    {
        $this->assertSame(1621358794, InvalidEncryptionKeyException::CODE);
    }
}
